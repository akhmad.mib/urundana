package com.bitsolution.urundana.core;

import com.haulmont.addons.cuba.jpa.repositories.config.EnableCubaRepositories;
import com.haulmont.addons.cuba.jpa.repositories.support.CubaRepositoryFactory;
import com.haulmont.addons.cuba.jpa.repositories.support.CubaRepositoryFactoryBean;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.global.Events;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

//@Order(Events.LOWEST_PLATFORM_PRECEDENCE + 100)
//@Configuration
//@ComponentScan("com.bitsolution.urundana.repository.master")
////@EnableJpaRepositories(
////        basePackages = {"com.bitsolution.urundana.repository.master"}
////        ,repositoryFactoryBeanClass = CubaRepositoryFactory.class
////)
//@EnableJpaRepositories(
//        basePackages = {"com.bitsolution.urundana.repository.master"}
////        ,repositoryFactoryBeanClass = CubaRepositoryFactory.class
//        ,repositoryBaseClass = RepositoryBaseImpl.class, repositoryFactoryBeanClass = CustomRepositoryFactoryBean.class
//)
public class JpaRepositoryConfig {

//    @Value("${value.from.file}")
//    String driverClassName;

//    @Value("${cuba.dataSource.jdbcUrl}")
//    String url;
//
//    @Value("${cuba.dataSource.username}")
//    String username;
//
//    @Value("${cuba.dataSource.pasword}")
//    String password;
//
//    @Inject
//    MemberConfig appConfig;
//
//
//    @Bean
//    public BasicDataSource getDataSource() {
//        BasicDataSource ds = new BasicDataSource();
////        ds.setDriverClassName(""); // you can call your code here
////        ds.setUrl(url); // to get these configuration values
////        ds.setUsername(username);
////        ds.setPassword(password);
//
//        ds.setUrl(appConfig.getJdbcUrl()); // to get these configuration values
//        ds.setUsername(appConfig.getUsername());
//        ds.setPassword(appConfig.getPassword());
//        return ds;
//    }
//
//    @Bean
//    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
//
//        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
//        vendorAdapter.setGenerateDdl(true);
//
//        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
//        factory.setJpaVendorAdapter(vendorAdapter);
//        factory.setPackagesToScan("com.bitsolution.urundana.entity.master","com.bitsolution.urundana.entity.userprofile","com.bitsolution.urundana.entity.base","com.bitsolution.urundana.entity.businessprofile");
//        factory.setDataSource(getDataSource());
//        return factory;
//    }
//
//    @Bean
//    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
//
//        JpaTransactionManager txManager = new JpaTransactionManager();
//        txManager.setEntityManagerFactory(entityManagerFactory);
//        return txManager;
//    }
}
