package com.bitsolution.urundana.config;

import com.haulmont.cuba.core.config.Config;
import com.haulmont.cuba.core.config.Property;
import com.haulmont.cuba.core.config.Source;
import com.haulmont.cuba.core.config.SourceType;
import com.haulmont.cuba.core.config.defaults.DefaultString;

@Source(type = SourceType.DATABASE)
public interface MemberConfig extends Config {

    @Property("urundana.member.roleId")
    String getRoleId();

    @Property("urundana.member.groupId")
    String getGroupId();

}
