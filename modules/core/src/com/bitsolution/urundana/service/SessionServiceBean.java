package com.bitsolution.urundana.service;

import com.haulmont.cuba.security.global.UserSession;
import org.springframework.stereotype.Service;

@Service(SessionService.NAME)
public class SessionServiceBean implements SessionService {

    @Override
    public boolean isAnonymous(UserSession userSession) {
        return userSession.getUser().getLogin().equals("anonymous");
    }
}