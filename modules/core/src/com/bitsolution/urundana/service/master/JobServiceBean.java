package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.Job;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.JobRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

@Service(JobService.NAME)
public class JobServiceBean extends BaseCrudServiceBean<Job,String, JobRepository> implements JobService {

}