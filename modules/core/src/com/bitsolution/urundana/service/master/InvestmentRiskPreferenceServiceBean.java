package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.InvestmentRiskPreference;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.InvestmentRiskPreferenceRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

@Service(InvestmentRiskPreferenceService.NAME)
public class InvestmentRiskPreferenceServiceBean extends BaseCrudServiceBean<InvestmentRiskPreference,String, InvestmentRiskPreferenceRepository> implements InvestmentRiskPreferenceService {

}