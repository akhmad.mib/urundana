package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.City;
import com.bitsolution.urundana.entity.master.Province;
import com.bitsolution.urundana.repository.master.CityRepository;
import com.bitsolution.urundana.repository.master.ProvinceRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(CityService.NAME)
public class CityServiceBean extends BaseCrudServiceBean<City,Integer, CityRepository> implements CityService {

    @Override
    public List<City> findAllByProvince(Province province) {
        return getRepository().findAllByProvince(province);
    }
}