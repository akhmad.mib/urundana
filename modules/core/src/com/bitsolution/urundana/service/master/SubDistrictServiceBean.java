package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.City;
import com.bitsolution.urundana.entity.master.Province;
import com.bitsolution.urundana.entity.master.SubDistrict;
import com.bitsolution.urundana.repository.master.ProvinceRepository;
import com.bitsolution.urundana.repository.master.SubDistrictRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(SubDistrictService.NAME)
public class SubDistrictServiceBean extends BaseCrudServiceBean<SubDistrict,Integer, SubDistrictRepository> implements SubDistrictService {

    @Override
    public List<SubDistrict> findAllByCity(City city) {
        return getRepository().findAllByCity(city);
    }
}