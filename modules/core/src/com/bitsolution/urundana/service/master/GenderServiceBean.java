package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

@Service(GenderService.NAME)
public class GenderServiceBean extends BaseCrudServiceBean<Gender,String, GenderRepository> implements GenderService {

}