package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.Nationality;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.NationalityRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

@Service(NationalityService.NAME)
public class NationalityServiceBean extends BaseCrudServiceBean<Nationality,String, NationalityRepository> implements NationalityService {

}