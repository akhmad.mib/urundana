package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.JobPosition;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.JobPositionRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

@Service(JobPositionService.NAME)
public class JobPositionServiceBean extends BaseCrudServiceBean<JobPosition,String, JobPositionRepository> implements JobPositionService {

}