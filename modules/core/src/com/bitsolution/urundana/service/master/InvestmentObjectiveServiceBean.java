package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.InvestmentObjective;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.InvestmentObjectiveRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

@Service(InvestmentObjectiveService.NAME)
public class InvestmentObjectiveServiceBean extends BaseCrudServiceBean<InvestmentObjective,String, InvestmentObjectiveRepository> implements InvestmentObjectiveService {

}