package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Education;
import com.bitsolution.urundana.entity.master.Province;
import com.bitsolution.urundana.repository.master.EducationRepository;
import com.bitsolution.urundana.repository.master.ProvinceRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

@Service(EducationService.NAME)
public class EducationServiceBean extends BaseCrudServiceBean<Education,String, EducationRepository> implements EducationService {

}