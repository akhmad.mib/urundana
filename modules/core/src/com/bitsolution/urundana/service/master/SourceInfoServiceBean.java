package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.SourceInfo;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.SourceInfoRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

@Service(SourceInfoService.NAME)
public class SourceInfoServiceBean extends BaseCrudServiceBean<SourceInfo,String, SourceInfoRepository> implements SourceInfoService {

}