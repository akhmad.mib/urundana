package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.MarketingFunnel;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.MarketingFunnelRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

@Service(MarketingFunnelService.NAME)
public class MarketingFunnelServiceBean extends BaseCrudServiceBean<MarketingFunnel,String, MarketingFunnelRepository> implements MarketingFunnelService {

}