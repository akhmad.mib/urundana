package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Nationality;
import com.bitsolution.urundana.entity.master.Province;
import com.bitsolution.urundana.repository.master.NationalityRepository;
import com.bitsolution.urundana.repository.master.ProvinceRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

@Service(ProvinceService.NAME)
public class ProvinceServiceBean extends BaseCrudServiceBean<Province,Integer, ProvinceRepository> implements ProvinceService {

}