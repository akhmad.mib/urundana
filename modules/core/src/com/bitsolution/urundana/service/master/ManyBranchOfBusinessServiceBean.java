package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.ManyBranchOfBusiness;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.ManyBranchOfBusinessRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

@Service(ManyBranchOfBusinessService.NAME)
public class ManyBranchOfBusinessServiceBean extends BaseCrudServiceBean<ManyBranchOfBusiness,String, ManyBranchOfBusinessRepository> implements ManyBranchOfBusinessService {

}