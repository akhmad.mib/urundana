package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.InvestmentRolePreference;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.InvestmentRolePreferenceRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

@Service(InvestmentRolePreferenceService.NAME)
public class InvestmentRolePreferenceServiceBean extends BaseCrudServiceBean<InvestmentRolePreference,String, InvestmentRolePreferenceRepository> implements InvestmentRolePreferenceService {

}