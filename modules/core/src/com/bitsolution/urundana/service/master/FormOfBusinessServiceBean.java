package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Education;
import com.bitsolution.urundana.entity.master.FormOfBusiness;
import com.bitsolution.urundana.repository.master.EducationRepository;
import com.bitsolution.urundana.repository.master.FormOfBusinessRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

@Service(FormOfBusinessService.NAME)
public class FormOfBusinessServiceBean extends BaseCrudServiceBean<FormOfBusiness,String, FormOfBusinessRepository> implements FormOfBusinessService {

}