package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Province;
import com.bitsolution.urundana.entity.master.UrbanVillage;
import com.bitsolution.urundana.repository.master.ProvinceRepository;
import com.bitsolution.urundana.repository.master.UrbanVillageRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

@Service(UrbanVillageService.NAME)
public class UrbanVillageServiceBean extends BaseCrudServiceBean<UrbanVillage,Integer, UrbanVillageRepository> implements UrbanVillageService {

}