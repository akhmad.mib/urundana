package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Bank;
import com.bitsolution.urundana.repository.master.BankRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service(BankService.NAME)
public class BankServiceBean extends BaseCrudServiceBean<Bank,String,BankRepository> implements BankService {

    @Inject
    DataManager dataManager;

    @Inject
    BankRepository bankRepository;

    @Override
    public List<Bank> getAll() {
        LoadContext<Bank> loadContext = LoadContext.create(Bank.class)
                .setQuery(
                        LoadContext.createQuery("select u from urundana_Bank u")
                )
                .setView("_local");
        List<Bank> list = dataManager.loadList(loadContext);
        System.out.println(list.size());
        return list;
    }

    @Override
    public Long countAll() {
        return bankRepository.countAll();
    }

    @Override
    public Bank findFirstByOrderByLabelDesc() {
        return bankRepository.findFirstByOrderByLabelDesc();
    }

    @Override
    public List<Bank> findByOrderByLabelAsc() {
        Pageable topTen = PageRequest.of(0, 10);
        return bankRepository.findAllByOrderByLabelAsc(topTen);
    }

    @Override
    public Page<Bank> findPageByOrderByLabelAsc() {
        Pageable topTen = PageRequest.of(0, 10);
        return bankRepository.findPageByOrderByLabelAsc(topTen);
    }
}