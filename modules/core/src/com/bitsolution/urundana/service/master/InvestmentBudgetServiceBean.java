package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.InvestmentBudget;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.InvestmentBudgetRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

@Service(InvestmentBudgetService.NAME)
public class InvestmentBudgetServiceBean extends BaseCrudServiceBean<InvestmentBudget,String, InvestmentBudgetRepository> implements InvestmentBudgetService {

}