package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Education;
import com.bitsolution.urundana.entity.master.Income;
import com.bitsolution.urundana.repository.master.EducationRepository;
import com.bitsolution.urundana.repository.master.IncomeRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

@Service(IncomeService.NAME)
public class IncomeServiceBean  extends BaseCrudServiceBean<Income,String, IncomeRepository> implements IncomeService {

}