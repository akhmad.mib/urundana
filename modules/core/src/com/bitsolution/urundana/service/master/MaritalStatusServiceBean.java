package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.MaritalStatus;
import com.bitsolution.urundana.entity.master.Nationality;
import com.bitsolution.urundana.repository.master.MaritalStatusRepository;
import com.bitsolution.urundana.repository.master.NationalityRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

@Service(MaritalStatusService.NAME)
public class MaritalStatusServiceBean extends BaseCrudServiceBean<MaritalStatus,String, MaritalStatusRepository> implements MaritalStatusService {

}