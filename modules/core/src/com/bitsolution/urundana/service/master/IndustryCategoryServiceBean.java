package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.IndustryCategory;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.IndustryCategoryRepository;
import com.bitsolution.urundana.service.base.BaseCrudServiceBean;
import org.springframework.stereotype.Service;

@Service(IndustryCategoryService.NAME)
public class IndustryCategoryServiceBean extends BaseCrudServiceBean<IndustryCategory,String, IndustryCategoryRepository> implements IndustryCategoryService {

}