package com.bitsolution.urundana.service;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.urundana.repository.master.*;
import com.haulmont.cuba.core.entity.Entity;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.Optional;

@Service(MasterRepositoryService.NAME)
public class MasterRepositoryServiceBean implements MasterRepositoryService {
    @Inject
    private BankRepository bankRepository;

    @Inject
    private CityRepository cityRepository;

    @Inject
    private EducationRepository educationRepository;

    @Inject
    private FormOfBusinessRepository formOfBusinessRepository;

    @Inject
    private GenderRepository genderRepository;

    @Inject
    private GroupRepository groupRepository;

    @Inject
    private IncomeRepository incomeRepository;

    @Inject
    private IncomeSourceRepository incomeSourceRepository;

    @Inject
    private IndustryCategoryRepository industryCategoryRepository;

    @Inject
    private InvestmentBudgetRepository investmentBudgetRepository;

    @Inject
    private InvestmentObjectiveRepository investmentObjectiveRepository;

    @Inject
    private InvestmentRiskPreferenceRepository investmentRiskPreferenceRepository;

    @Inject
    private InvestmentRolePreferenceRepository investmentRolePreferenceRepository;

    @Inject
    private JobPositionRepository jobPositionRepository;

    @Inject
    private JobRepository jobRepository;

    @Inject
    private ManyBranchOfBusinessRepository manyBranchOfBusinessRepository;

    @Inject
    private MaritalStatusRepository maritalStatusRepository;

    @Inject
    private MarketingFunnelRepository marketingFunnelRepository;

    @Inject
    private NationalityRepository nationalityRepository;

    @Inject
    private ProvinceRepository provinceRepository;

    @Inject
    private RoleRepository roleRepository;

    @Inject
    private SourceInfoRepository sourceInfoRepository;

    @Inject
    private SubDistrictRepository subDistrictRepository;

    @Inject
    private UrbanVillageRepository urbanVillageRepository;

    @Inject
    private UserRepository userRepository;

    protected SimpleCubaJpaRepository getRepository(String name) {
        switch (name) {
            case MasterRepositoryService.REPO_BANK:
                return getBankRepository();
            case MasterRepositoryService.REPO_CITY:
                return getCityRepository();
            case MasterRepositoryService.REPO_EDUCATION:
                return getEducationRepository();
            case MasterRepositoryService.REPO_FORM_OF_BUSINESS:
                return getFormOfBusinessRepository();
            case MasterRepositoryService.REPO_GENDER:
                return getGenderRepository();
            case MasterRepositoryService.REPO_INCOME:
                return getIncomeRepository();
            case MasterRepositoryService.REPO_INCOME_SOURCE:
                return getIncomeSourceRepository();
            case MasterRepositoryService.REPO_INDUSTRY_CATEGORY:
                return getIndustryCategoryRepository();
            case MasterRepositoryService.REPO_INVESTMENT_BUDGET:
                return getInvestmentBudgetRepository();
            case MasterRepositoryService.REPO_INVESTMENT_OBJECTIVE:
                return getInvestmentObjectiveRepository();
            case MasterRepositoryService.REPO_INVESTMENT_RISK_PREFERENCE:
                return getInvestmentRiskPreferenceRepository();
            case MasterRepositoryService.REPO_INVESTMENT_ROLE_PREFERENCE:
                return getInvestmentRolePreferenceRepository();
            case MasterRepositoryService.REPO_JOB_POSITION:
                return getJobPositionRepository();
            case MasterRepositoryService.REPO_JOB:
                return getJobRepository();
            case MasterRepositoryService.REPO_MANY_BRANCH_OF_BUSINESS:
                return getManyBranchOfBusinessRepository();
            case MasterRepositoryService.REPO_MARITAL_STATUS:
                return getMaritalStatusRepository();
            case MasterRepositoryService.REPO_MARKETING_FUNNEL:
                return getMarketingFunnelRepository();
            case MasterRepositoryService.REPO_NATIONALITY:
                return getNationalityRepository();
            case MasterRepositoryService.REPO_PROVINCE:
                return getProvinceRepository();
            case MasterRepositoryService.REPO_SOURCE_INFO:
                return getSourceInfoRepository();
            case MasterRepositoryService.REPO_SUB_DISTRICT:
                return getSubDistrictRepository();
            case MasterRepositoryService.REPO_URBAN_VILLAGE:
                return getUrbanVillageRepository();
            case MasterRepositoryService.REPO_GROUP:
                return getGroupRepository();
            case MasterRepositoryService.REPO_ROLE:
                return getRoleRepository();
            case MasterRepositoryService.REPO_USER:
                return getUserRepository();
            default: return getUserRepository();
        }
    }

    public BankRepository getBankRepository() {
        return bankRepository;
    }

    public void setBankRepository(BankRepository bankRepository) {
        this.bankRepository = bankRepository;
    }

    public CityRepository getCityRepository() {
        return cityRepository;
    }

    public void setCityRepository(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    public EducationRepository getEducationRepository() {
        return educationRepository;
    }

    public void setEducationRepository(EducationRepository educationRepository) {
        this.educationRepository = educationRepository;
    }

    public FormOfBusinessRepository getFormOfBusinessRepository() {
        return formOfBusinessRepository;
    }

    public void setFormOfBusinessRepository(FormOfBusinessRepository formOfBusinessRepository) {
        this.formOfBusinessRepository = formOfBusinessRepository;
    }

    public GenderRepository getGenderRepository() {
        return genderRepository;
    }

    public void setGenderRepository(GenderRepository genderRepository) {
        this.genderRepository = genderRepository;
    }

    public GroupRepository getGroupRepository() {
        return groupRepository;
    }

    public void setGroupRepository(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    public IncomeRepository getIncomeRepository() {
        return incomeRepository;
    }

    public void setIncomeRepository(IncomeRepository incomeRepository) {
        this.incomeRepository = incomeRepository;
    }

    public IncomeSourceRepository getIncomeSourceRepository() {
        return incomeSourceRepository;
    }

    public void setIncomeSourceRepository(IncomeSourceRepository incomeSourceRepository) {
        this.incomeSourceRepository = incomeSourceRepository;
    }

    public IndustryCategoryRepository getIndustryCategoryRepository() {
        return industryCategoryRepository;
    }

    public void setIndustryCategoryRepository(IndustryCategoryRepository industryCategoryRepository) {
        this.industryCategoryRepository = industryCategoryRepository;
    }

    public InvestmentBudgetRepository getInvestmentBudgetRepository() {
        return investmentBudgetRepository;
    }

    public void setInvestmentBudgetRepository(InvestmentBudgetRepository investmentBudgetRepository) {
        this.investmentBudgetRepository = investmentBudgetRepository;
    }

    public InvestmentObjectiveRepository getInvestmentObjectiveRepository() {
        return investmentObjectiveRepository;
    }

    public void setInvestmentObjectiveRepository(InvestmentObjectiveRepository investmentObjectiveRepository) {
        this.investmentObjectiveRepository = investmentObjectiveRepository;
    }

    public InvestmentRiskPreferenceRepository getInvestmentRiskPreferenceRepository() {
        return investmentRiskPreferenceRepository;
    }

    public void setInvestmentRiskPreferenceRepository(InvestmentRiskPreferenceRepository investmentRiskPreferenceRepository) {
        this.investmentRiskPreferenceRepository = investmentRiskPreferenceRepository;
    }

    public InvestmentRolePreferenceRepository getInvestmentRolePreferenceRepository() {
        return investmentRolePreferenceRepository;
    }

    public void setInvestmentRolePreferenceRepository(InvestmentRolePreferenceRepository investmentRolePreferenceRepository) {
        this.investmentRolePreferenceRepository = investmentRolePreferenceRepository;
    }

    public JobPositionRepository getJobPositionRepository() {
        return jobPositionRepository;
    }

    public void setJobPositionRepository(JobPositionRepository jobPositionRepository) {
        this.jobPositionRepository = jobPositionRepository;
    }

    public JobRepository getJobRepository() {
        return jobRepository;
    }

    public void setJobRepository(JobRepository jobRepository) {
        this.jobRepository = jobRepository;
    }

    public ManyBranchOfBusinessRepository getManyBranchOfBusinessRepository() {
        return manyBranchOfBusinessRepository;
    }

    public void setManyBranchOfBusinessRepository(ManyBranchOfBusinessRepository manyBranchOfBusinessRepository) {
        this.manyBranchOfBusinessRepository = manyBranchOfBusinessRepository;
    }

    public MaritalStatusRepository getMaritalStatusRepository() {
        return maritalStatusRepository;
    }

    public void setMaritalStatusRepository(MaritalStatusRepository maritalStatusRepository) {
        this.maritalStatusRepository = maritalStatusRepository;
    }

    public MarketingFunnelRepository getMarketingFunnelRepository() {
        return marketingFunnelRepository;
    }

    public void setMarketingFunnelRepository(MarketingFunnelRepository marketingFunnelRepository) {
        this.marketingFunnelRepository = marketingFunnelRepository;
    }

    public NationalityRepository getNationalityRepository() {
        return nationalityRepository;
    }

    public void setNationalityRepository(NationalityRepository nationalityRepository) {
        this.nationalityRepository = nationalityRepository;
    }

    public ProvinceRepository getProvinceRepository() {
        return provinceRepository;
    }

    public void setProvinceRepository(ProvinceRepository provinceRepository) {
        this.provinceRepository = provinceRepository;
    }

    public RoleRepository getRoleRepository() {
        return roleRepository;
    }

    public void setRoleRepository(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public SourceInfoRepository getSourceInfoRepository() {
        return sourceInfoRepository;
    }

    public void setSourceInfoRepository(SourceInfoRepository sourceInfoRepository) {
        this.sourceInfoRepository = sourceInfoRepository;
    }

    public SubDistrictRepository getSubDistrictRepository() {
        return subDistrictRepository;
    }

    public void setSubDistrictRepository(SubDistrictRepository subDistrictRepository) {
        this.subDistrictRepository = subDistrictRepository;
    }

    public UrbanVillageRepository getUrbanVillageRepository() {
        return urbanVillageRepository;
    }

    public void setUrbanVillageRepository(UrbanVillageRepository urbanVillageRepository) {
        this.urbanVillageRepository = urbanVillageRepository;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Entity findOne(String repositoryName, Serializable serializable, String view) {
        return getRepository(repositoryName).findOne(serializable,view);
    }

    @Override
    public Iterable findAll(String repositoryName, String view) {
        return getRepository(repositoryName).findAll(view);
    }

    @Override
    public Iterable findAll(String repositoryName, Iterable iterable, String view) {
        return getRepository(repositoryName).findAll(iterable,view);
    }

    @Override
    public String getDefaultViewName(String repositoryName) {
        return getRepository(repositoryName).getDefaultViewName();
    }

    @Override
    public void setDefaultViewName(String repositoryName, String defaultViewName) {
        getRepository(repositoryName).setDefaultViewName(defaultViewName);
    }

    @Override
    public Object save(String repositoryName, Object entity) {
        return getRepository(repositoryName).save(entity);
    }

    @Override
    public Iterable saveAll(String repositoryName, Iterable entities) {
        return getRepository(repositoryName).saveAll(entities);
    }

    @Override
    public Optional findById(String repositoryName, Object o) {
        return getRepository(repositoryName).findById(o);
    }

    @Override
    public boolean existsById(String repositoryName, Object o) {
        return getRepository(repositoryName).existsById(o);
    }

    @Override
    public Iterable findAll(String repositoryName) {
        return getRepository(repositoryName).findAll();
    }

    @Override
    public Iterable findAllById(String repositoryName, Iterable iterable) {
        return getRepository(repositoryName).findAllById(iterable);
    }

    @Override
    public long count(String repositoryName) {
        return getRepository(repositoryName).count();
    }

    @Override
    public void deleteById(String repositoryName, Object o) {
        getRepository(repositoryName).deleteById(o);
    }

    @Override
    public void delete(String repositoryName, Object entity) {
        getRepository(repositoryName).delete(entity);
    }

    @Override
    public void deleteAll(String repositoryName, Iterable entities) {
        getRepository(repositoryName).deleteAll(entities);
    }

    @Override
    public void deleteAll(String repositoryName) {
        getRepository(repositoryName).deleteAll();
    }
}