package com.bitsolution.urundana.service.userprofile;

import com.bitsolution.urundana.config.MemberConfig;
import com.bitsolution.urundana.dto.userprofile.RegisterUserDto;
import com.bitsolution.urundana.entity.userprofile.*;
import com.bitsolution.urundana.repository.master.UserRepository;
import com.bitsolution.urundana.result.userprofile.RegisterUserActionResult;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Transaction;
import com.haulmont.cuba.core.global.*;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.entity.Role;
import com.haulmont.cuba.security.entity.User;
import com.haulmont.cuba.security.entity.UserRole;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.UUID;

@Service(RegisterService.NAME)
public class RegisterServiceBean implements RegisterService {

    @Inject
    UserRepository userRepository;

    @Inject
    MemberConfig memberConfig;

    @Inject
    private DataManager dataManager;

    @Inject
    private Metadata metadata;

    @Inject
    private PasswordEncryption passwordEncryption;

    @Inject
    private Persistence persistence;

    @Override
    public RegisterUserActionResult registerUser(RegisterUserDto registerUserDto) {
        RegisterUserActionResult actionResult = new RegisterUserActionResult();
        Transaction tx = persistence.createTransaction();
        try {
            EntityManager em = persistence.getEntityManager();
//            Group group = dataManager.load(LoadContext.create(Group.class).setId(UUID.fromString(memberConfig.getGroupId())));
//            Role role = dataManager.load(LoadContext.create(Role.class).setId(UUID.fromString(memberConfig.getRoleId())));
            // Create a user instance
            User user = metadata.create(User.class);
            user.setLogin(registerUserDto.getEmail());
            user.setPassword(passwordEncryption.getPasswordHash(user.getId(), registerUserDto.getPassword()));
            user.setFirstName(registerUserDto.getFirstName());
            user.setLastName(registerUserDto.getLastName());
            user.setEmail(registerUserDto.getEmail());
            // Note that the platform does not support the default group out of the box, so here we define the default group id and set it for the newly registered users.
//            user.setGroup(group);

            /* Create a link to the role
             * Here we programmatically set the default role.
             * Another way is to set the default role by using the DB scripts. Set IS_DEFAULT_ROLE parameter to true in the insert script for the role.
             * Also, this parameter might be changed in the Role Editor screen.
             */
            user.setActive(true);
            user.setLanguage("en");
            user.setChangePasswordAtNextLogon(false);
            user.setGroup(em.getReference(Group.class, UUID.fromString(memberConfig.getGroupId())));

            UserRole userRole = metadata.create(UserRole.class);
            userRole.setUser(user);
            userRole.setRole(em.getReference(Role.class, UUID.fromString(memberConfig.getRoleId())));

            em.persist(user);

            PersonalDataProfile personalDataProfile = metadata.create(PersonalDataProfile.class);
            personalDataProfile.setUser(user);
            personalDataProfile.setFirstName(registerUserDto.getFirstName());
            personalDataProfile.setLastName(registerUserDto.getLastName());
            personalDataProfile.setPhoneNumber(registerUserDto.getPhoneNumber());

            JobProfile jobProfile = metadata.create(JobProfile.class);;
            jobProfile.setUser(user);

            DocumentProfile documentProfile = metadata.create(DocumentProfile.class);
            documentProfile.setUser(user);

            BankProfile bankProfile = metadata.create(BankProfile.class);
            bankProfile.setUser(user);

            PreferenceProfile preferenceProfile = metadata.create(PreferenceProfile.class);
            preferenceProfile.setUser(user);

            UserProfile userProfile = metadata.create(UserProfile.class);
            userProfile.setUser(user);
            userProfile.setPersonalData(personalDataProfile);
            userProfile.setJob(jobProfile);
            userProfile.setDocument(documentProfile);
            userProfile.setBank(bankProfile);
            userProfile.setPreference(preferenceProfile);
            userProfile.setHasFinish(false);
            userProfile.setStep(1);

            em.persist(userProfile);

            tx.commit();
            actionResult.setSuccess(true);
        }catch (Exception ex){
            ex.printStackTrace();
            actionResult.setSuccess(false);
            actionResult.setMessage(ex.getMessage());
        }finally {
            tx.end();
        }
        return actionResult;
    }
}