package com.bitsolution.urundana.service.userprofile;

import com.bitsolution.urundana.dto.userprofile.ProfileDto;
import com.bitsolution.urundana.entity.userprofile.Profile;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Service(ProfileService.NAME)
public class ProfileServiceBean implements ProfileService {
    @Inject
    DataManager dataManager;

    @Override
    public List<Profile> getAll() {
        LoadContext<Profile> loadContext = LoadContext.create(Profile.class)
                .setQuery(
                        LoadContext.createQuery("select u from urundana_Profile u")
                )
                .setView("profile-view");
        List<Profile> list = dataManager.loadList(loadContext);
        System.out.println(list.size());
        return list;
    }

    @Override
    public List<ProfileDto> getAllDto() {
        ModelMapper modelMapper = new ModelMapper();
        List<ProfileDto> dtos = getAll()
                .stream()
                .map(profile -> modelMapper.map(profile, ProfileDto.class))
                .collect(Collectors.toList());
        return dtos;
    }
}