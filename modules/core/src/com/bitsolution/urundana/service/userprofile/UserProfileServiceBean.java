package com.bitsolution.urundana.service.userprofile;

import com.bitsolution.urundana.entity.userprofile.BankProfile;
import com.bitsolution.urundana.entity.userprofile.UserProfile;
import com.bitsolution.urundana.repository.userprofile.UserProfileRepository;
import com.bitsolution.urundana.service.base.BaseByUserServiceBean;
import org.springframework.stereotype.Service;

@Service(UserProfileService.NAME)
public class UserProfileServiceBean extends BaseByUserServiceBean<UserProfile, String, UserProfileRepository> implements UserProfileService {

}