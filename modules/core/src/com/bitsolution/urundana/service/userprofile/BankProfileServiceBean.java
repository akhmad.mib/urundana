package com.bitsolution.urundana.service.userprofile;

import com.bitsolution.urundana.entity.userprofile.BankProfile;
import com.bitsolution.urundana.repository.userprofile.BankProfileRepository;
import com.bitsolution.urundana.service.base.BaseByUserServiceBean;
import org.springframework.stereotype.Service;

@Service(BankProfileService.NAME)
public class BankProfileServiceBean extends BaseByUserServiceBean<BankProfile,String, BankProfileRepository> implements BankProfileService {

}