package com.bitsolution.urundana.service.userprofile;

import com.bitsolution.urundana.entity.userprofile.BankProfile;
import com.bitsolution.urundana.entity.userprofile.JobProfile;
import com.bitsolution.urundana.repository.userprofile.BankProfileRepository;
import com.bitsolution.urundana.repository.userprofile.JobProfileRepository;
import com.bitsolution.urundana.service.base.BaseByUserServiceBean;
import org.springframework.stereotype.Service;

@Service(JobProfileService.NAME)
public class JobProfileServiceBean extends BaseByUserServiceBean<JobProfile,String, JobProfileRepository> implements JobProfileService {

}