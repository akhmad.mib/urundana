package com.bitsolution.urundana.service.userprofile;

import com.bitsolution.urundana.entity.userprofile.BankProfile;
import com.bitsolution.urundana.entity.userprofile.DocumentProfile;
import com.bitsolution.urundana.repository.userprofile.BankProfileRepository;
import com.bitsolution.urundana.repository.userprofile.DocumentProfileRepository;
import com.bitsolution.urundana.service.base.BaseByUserServiceBean;
import org.springframework.stereotype.Service;

@Service(DocumentProfileService.NAME)
public class DocumentProfileServiceBean extends BaseByUserServiceBean<DocumentProfile,String, DocumentProfileRepository> implements DocumentProfileService {

}