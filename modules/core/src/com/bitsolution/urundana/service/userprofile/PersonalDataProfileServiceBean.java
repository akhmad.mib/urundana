package com.bitsolution.urundana.service.userprofile;

import com.bitsolution.urundana.entity.userprofile.BankProfile;
import com.bitsolution.urundana.entity.userprofile.PersonalDataProfile;
import com.bitsolution.urundana.repository.userprofile.BankProfileRepository;
import com.bitsolution.urundana.repository.userprofile.PersonalDataProfileRepository;
import com.bitsolution.urundana.service.base.BaseByUserServiceBean;
import org.springframework.stereotype.Service;

@Service(PersonalDataProfileService.NAME)
public class PersonalDataProfileServiceBean extends BaseByUserServiceBean<PersonalDataProfile,String, PersonalDataProfileRepository> implements PersonalDataProfileService {

}