package com.bitsolution.urundana.service.userprofile;

import com.bitsolution.urundana.entity.userprofile.BankProfile;
import com.bitsolution.urundana.entity.userprofile.PreferenceProfile;
import com.bitsolution.urundana.repository.userprofile.BankProfileRepository;
import com.bitsolution.urundana.repository.userprofile.PreferenceProfileRepository;
import com.bitsolution.urundana.service.base.BaseByUserServiceBean;
import org.springframework.stereotype.Service;

@Service(PreferenceProfileService.NAME)
public class PreferenceProfileServiceBean extends BaseByUserServiceBean<PreferenceProfile,String, PreferenceProfileRepository> implements PreferenceProfileService {

}