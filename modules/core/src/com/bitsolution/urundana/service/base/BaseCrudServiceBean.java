package com.bitsolution.urundana.service.base;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.DataManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Optional;

@Service(BaseCrudService.NAME)
public class BaseCrudServiceBean<T extends Entity,ID extends Serializable, R extends SimpleCubaJpaRepository> implements BaseCrudService<T,ID,R> {

    @Inject
    DataManager dataManager;

    @Inject
    Persistence persistence;


    protected R getRepository(){
        return (R) AppBeans.get(getGenericRepositoryClass());
    }


    protected Class<T> getGenericEntityClass() {
        Class<T> classT = (Class<T>)
                ((ParameterizedType) getClass().getGenericSuperclass())
                        .getActualTypeArguments()[0];
        System.out.println(classT.getName());
        System.out.println(classT.getSimpleName());
        return classT;
    }

    protected Class<T> getGenericRepositoryClass() {
        Class<T> classT = (Class<T>)
                ((ParameterizedType) getClass().getGenericSuperclass())
                        .getActualTypeArguments()[2];
        System.out.println(classT.getName());
        System.out.println(classT.getSimpleName());
        return classT;
    }

    public DataManager getDataManager(){
        return dataManager;
    }

    @Override
    public Entity findOne(Serializable serializable, String view) {
        return getRepository().findOne(serializable,view);
    }

    @Override
    public Iterable findAll(String view) {
        return getRepository().findAll(view);
    }

    @Override
    public Iterable findAll(Iterable iterable, String view) {
        return getRepository().findAll(iterable,view);
    }

    @Override
    public String getDefaultViewName() {
        return getRepository().getDefaultViewName();
    }

    @Override
    public void setDefaultViewName(String defaultViewName) {
        getRepository().setDefaultViewName(defaultViewName);
    }

    @Override
    public Object save(Object entity) {
        return getRepository().save(entity);
    }

    @Override
    public Iterable saveAll(Iterable entities) {
        return getRepository().saveAll(entities);
    }

    @Override
    public Optional findById(Object o) {
        return getRepository().findById(o);
    }

    @Override
    public boolean existsById(Object o) {
        return getRepository().existsById(o);
    }

    @Override
    public Iterable findAll() {
        return getRepository().findAll();
    }

    @Override
    public Iterable findAllById(Iterable iterable) {
        return getRepository().findAllById(iterable);
    }

    @Override
    public long count() {
        return getRepository().count();
    }

    @Override
    public void deleteById(Object o) {
        getRepository().deleteById(o);
    }

    @Override
    public void delete(Object entity) {
        getRepository().delete(entity);
    }

    @Override
    public void deleteAll(Iterable entities) {
        getRepository().deleteAll(entities);
    }

    @Override
    public void deleteAll() {
        getRepository().deleteAll();
    }

    @Override
    @Transactional
    public void persist(T entity) {
        persistence.getEntityManager().merge(entity);
    }
}