package com.bitsolution.urundana.service.base;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.core.global.View;
import com.haulmont.cuba.security.entity.User;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.UUID;

@Service(BaseByUserService.NAME)
public class BaseByUserServiceBean<T extends Entity,ID extends Serializable, R extends SimpleCubaJpaRepository> extends BaseCrudServiceBean<T,ID,R> implements BaseByUserService<T,ID,R> {

    @Override
    public T findByUser(User user) {
        return findByUser(user, View.LOCAL);
    }

    @Override
    public T findByUserId(UUID userId) {
        return findByUserId(userId, View.LOCAL);
    }

    @Override
    public T findByUser(User user, String view) {
        try {
            return (T) getDataManager().load(getGenericEntityClass()).query("e.user = :user")
                    .parameter("user", user).view(view).one();
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public T findByUserId(UUID userId, String view) {
        try {
            return (T) getDataManager().load(getGenericEntityClass()).query("e.user.id = :userId")
                    .parameter("userId", userId).view(view).one();
        } catch (Exception ex) {
            return null;
        }
    }
}