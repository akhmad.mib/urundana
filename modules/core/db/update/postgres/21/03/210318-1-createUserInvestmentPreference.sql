create table URUNDANA_USER_INVESTMENT_PREFERENCE (
    ID varchar(255),
    UUID uuid,
    --
    USER_ID uuid,
    INVESTMENT_PREFERENCE_ID varchar(255),
    PREFERENCE_PROFILE_ID varchar(255),
    PROFILE_ID varchar(255),
    --
    primary key (ID)
);