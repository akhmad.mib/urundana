create table URUNDANA_MARKETING_FUNNEL (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
);