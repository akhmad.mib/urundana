create table URUNDANA_USER_PROFILE (
    ID varchar(255),
    UUID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    --
    USER_ID uuid,
    PERSONAL_DATA_ID varchar(255),
    JOB_ID varchar(255),
    DOCUMENT_ID varchar(255),
    BANK_ID varchar(255),
    PREFERENCE_ID varchar(255),
    STEP integer,
    HAS_FINISH boolean,
    --
    primary key (ID)
);