create table URUNDANA_URBAN_VILLAGE (
    ID integer,
    --
    PROVINCE_ID integer,
    PROVINCE_NAME varchar(100),
    CITY_ID integer,
    CITY_NAME varchar(100),
    SUB_DISTRICT_ID integer,
    SUB_DISTRICT_NAME varchar(100),
    NAME varchar(100) not null,
    TYPE_OF_AREA varchar(50),
    --
    primary key (ID)
);