create table URUNDANA_USER_INVESTMENT_OBJECTIVE (
    ID varchar(255),
    UUID uuid,
    --
    USER_ID uuid,
    INVESTMENT_OBJECTIVE_ID varchar(255),
    PREFERENCE_PROFILE_ID varchar(255),
    PROFILE_ID varchar(255),
    --
    primary key (ID)
);