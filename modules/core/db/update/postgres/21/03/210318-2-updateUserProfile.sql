alter table URUNDANA_USER_PROFILE rename column job_id to job_id__u14163 ;
alter table URUNDANA_USER_PROFILE drop constraint FK_URUNDANA_USER_PROFILE_ON_JOB ;
drop index IDX_URUNDANA_USER_PROFILE_ON_JOB ;
alter table URUNDANA_USER_PROFILE add column UUID uuid ;
alter table URUNDANA_USER_PROFILE add column JOB_ID varchar(255) ;
