alter table URUNDANA_PERSONAL_DATA_PROFILE rename column married_status_id to married_status_id__u11979 ;
drop index IDX_URUNDANA_PERSONAL_DATA_PROFILE_ON_MARRIED_STATUS ;
alter table URUNDANA_PERSONAL_DATA_PROFILE rename column deleted_by to deleted_by__u09896 ;
alter table URUNDANA_PERSONAL_DATA_PROFILE rename column delete_ts to delete_ts__u85365 ;
alter table URUNDANA_PERSONAL_DATA_PROFILE add column MARRIED_STATUS_ID varchar(255) ;
