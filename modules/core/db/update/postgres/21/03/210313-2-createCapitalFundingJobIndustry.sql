alter table URUNDANA_CAPITAL_FUNDING_JOB_INDUSTRY add constraint FK_URUNDANA_CAPITAL_FUNDING_JOB_INDUSTRY_ON_CAPITAL_FUNDING foreign key (CAPITAL_FUNDING_ID) references URUNDANA_CAPITAL_FUNDING(ID);
alter table URUNDANA_CAPITAL_FUNDING_JOB_INDUSTRY add constraint FK_URUNDANA_CAPITAL_FUNDING_JOB_INDUSTRY_ON_JOBINDUSTRY foreign key (JOBINDUSTRY_ID) references URUNDANA_INDUSTRY_CATEGORY(ID);
create index IDX_URUNDANA_CAPITAL_FUNDING_JOB_INDUSTRY_ON_CAPITAL_FUNDING on URUNDANA_CAPITAL_FUNDING_JOB_INDUSTRY (CAPITAL_FUNDING_ID);
create index IDX_URUNDANA_CAPITAL_FUNDING_JOB_INDUSTRY_ON_JOBINDUSTRY on URUNDANA_CAPITAL_FUNDING_JOB_INDUSTRY (JOBINDUSTRY_ID);
