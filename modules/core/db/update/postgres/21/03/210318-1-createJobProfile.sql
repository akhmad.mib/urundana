create table URUNDANA_JOB_PROFILE (
    ID varchar(255),
    UUID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    --
    USER_ID uuid,
    LAST_EDUCATION_ID varchar(255),
    JOB_ID varchar(255),
    INDUSTRY_CATEGORY_ID varchar(255),
    INCOME_ID varchar(255),
    INCOME_SOURCE_ID varchar(255),
    BUSINESS_DESCRIPTION text,
    INCOME_SOURCE_DESCRIPTION text,
    --
    primary key (ID)
);