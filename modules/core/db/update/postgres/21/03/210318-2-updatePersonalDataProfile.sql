alter table URUNDANA_PERSONAL_DATA_PROFILE add column UUID uuid ;
alter table URUNDANA_PERSONAL_DATA_PROFILE alter column PHONE_NUMBER set data type varchar(15) ;
alter table URUNDANA_PERSONAL_DATA_PROFILE alter column LANDLINE_NUMBER set data type varchar(15) ;
