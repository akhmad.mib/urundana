create table URUNDANA_PERSONAL_DATA_PROFILE (
    ID varchar(255),
    UUID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    --
    USER_ID uuid,
    FIRST_NAME varchar(50) not null,
    LAST_NAME varchar(50),
    GENDER_ID varchar(255),
    BIRTH_OF_PLACE varchar(100),
    BIRTH_OF_DATE date,
    NATIONALITY_ID varchar(255),
    MARITAL_STATUS_ID varchar(255),
    MOTHER_NAME varchar(100),
    PROFIL_BACKGROUND text,
    PHONE_NUMBER varchar(15),
    LANDLINE_NUMBER varchar(15),
    ID_CARD_ADDRESS text,
    ID_CARD_PROVINCE_ID integer,
    ID_CARD_CITY_ID integer,
    ID_CARD_SUB_DISTRICT_ID integer,
    ID_CARD_URBAN_VILLAGE varchar(100),
    ID_CARD_RT integer,
    ID_CARD_RW integer,
    HOME_ADDRESS text,
    HOME_PROVINCE_ID integer,
    HOME_CITY_ID integer,
    HOME_SUB_DISTRICT_ID integer,
    HOME_URBAN_VILLAGE varchar(100),
    HOME_RT integer,
    HOME_RW integer,
    --
    primary key (ID)
);