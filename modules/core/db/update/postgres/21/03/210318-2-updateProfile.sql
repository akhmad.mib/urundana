alter table URUNDANA_PROFILE rename column landline_number to landline_number__u46982 ;
alter table URUNDANA_PROFILE rename column phone_number to phone_number__u37670 ;
alter table URUNDANA_PROFILE rename column married_status_id to married_status_id__u07259 ;
alter table URUNDANA_PROFILE drop constraint FK_URUNDANA_PROFILE_ON_MARRIED_STATUS ;
drop index IDX_URUNDANA_PROFILE_ON_MARRIED_STATUS ;
alter table URUNDANA_PROFILE add column UUID uuid ;
alter table URUNDANA_PROFILE add column MARITAL_STATUS_ID varchar(255) ;
alter table URUNDANA_PROFILE add column PHONE_NUMBER varchar(15) ;
alter table URUNDANA_PROFILE add column LANDLINE_NUMBER varchar(15) ;
