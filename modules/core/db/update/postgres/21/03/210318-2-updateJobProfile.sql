alter table URUNDANA_JOB_PROFILE rename column deleted_by to deleted_by__u70924 ;
alter table URUNDANA_JOB_PROFILE rename column delete_ts to delete_ts__u51478 ;
alter table URUNDANA_JOB_PROFILE rename column id to id__u17474 ;
alter table URUNDANA_JOB_PROFILE alter column id__u17474 drop not null ;
alter table URUNDANA_JOB_PROFILE add column UUID uuid ;
alter table URUNDANA_JOB_PROFILE add column ID varchar(255) ^
update URUNDANA_JOB_PROFILE set ID = '' where ID is null ;
alter table URUNDANA_JOB_PROFILE alter column ID set not null ;
