create table URUNDANA_PREFERENCE_PROFILE (
    ID varchar(255),
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    --
    USER_ID uuid,
    INVESTMENT_BUDGET_ID varchar(255),
    INVESTMENT_RISK_PREFERENCE_ID varchar(255),
    INVESTMENT_ROLE_PREFERENCE_ID varchar(255),
    SOURCE_INFO_ID varchar(255),
    --
    primary key (ID)
);