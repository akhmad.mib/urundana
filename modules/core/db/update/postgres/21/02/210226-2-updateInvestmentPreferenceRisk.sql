alter table URUNDANA_INVESTMENT_PREFERENCE_RISK rename column id to id__u06313 ;
alter table URUNDANA_INVESTMENT_PREFERENCE_RISK alter column id__u06313 drop not null ;
alter table URUNDANA_INVESTMENT_PREFERENCE_RISK add column ID varchar(255) ^
update URUNDANA_INVESTMENT_PREFERENCE_RISK set ID = '' where ID is null ;
alter table URUNDANA_INVESTMENT_PREFERENCE_RISK alter column ID set not null ;
