alter table URUNDANA_INCOME_SOURCE rename column id to id__u12099 ;
alter table URUNDANA_INCOME_SOURCE alter column id__u12099 drop not null ;
alter table URUNDANA_INCOME_SOURCE add column ID varchar(255) ^
update URUNDANA_INCOME_SOURCE set ID = '' where ID is null ;
alter table URUNDANA_INCOME_SOURCE alter column ID set not null ;
