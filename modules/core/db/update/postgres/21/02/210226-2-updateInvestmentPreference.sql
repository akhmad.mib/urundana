alter table URUNDANA_INVESTMENT_PREFERENCE rename column id to id__u83732 ;
alter table URUNDANA_INVESTMENT_PREFERENCE alter column id__u83732 drop not null ;
alter table URUNDANA_INVESTMENT_PREFERENCE add column ID varchar(255) ^
update URUNDANA_INVESTMENT_PREFERENCE set ID = '' where ID is null ;
alter table URUNDANA_INVESTMENT_PREFERENCE alter column ID set not null ;
