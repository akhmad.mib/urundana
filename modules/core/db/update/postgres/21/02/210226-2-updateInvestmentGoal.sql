alter table URUNDANA_INVESTMENT_GOAL rename column id to id__u74753 ;
alter table URUNDANA_INVESTMENT_GOAL alter column id__u74753 drop not null ;
alter table URUNDANA_INVESTMENT_GOAL add column ID varchar(255) ^
update URUNDANA_INVESTMENT_GOAL set ID = '' where ID is null ;
alter table URUNDANA_INVESTMENT_GOAL alter column ID set not null ;
