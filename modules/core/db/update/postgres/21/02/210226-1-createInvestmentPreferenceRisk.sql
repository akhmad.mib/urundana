create table URUNDANA_INVESTMENT_PREFERENCE_RISK (
    ID bigint,
    --
    CODE varchar(100) not null,
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
);