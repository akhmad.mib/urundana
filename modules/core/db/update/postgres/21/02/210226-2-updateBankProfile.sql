alter table URUNDANA_BANK_PROFILE rename column bank_id to bank_id__u57807 ;
alter table URUNDANA_BANK_PROFILE drop constraint FK_URUNDANA_BANK_PROFILE_ON_BANK ;
drop index IDX_URUNDANA_BANK_PROFILE_ON_BANK ;
alter table URUNDANA_BANK_PROFILE rename column id to id__u09789 ;
alter table URUNDANA_BANK_PROFILE alter column id__u09789 drop not null ;
alter table URUNDANA_BANK_PROFILE add column ID varchar(255) ^
update URUNDANA_BANK_PROFILE set ID = '' where ID is null ;
alter table URUNDANA_BANK_PROFILE alter column ID set not null ;
alter table URUNDANA_BANK_PROFILE add column BANK_ID varchar(255) ;
