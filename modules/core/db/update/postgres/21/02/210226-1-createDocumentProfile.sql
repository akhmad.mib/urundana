create table URUNDANA_DOCUMENT_PROFILE (
    ID bigint,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    SID_NUMBER varchar(100),
    ID_CARD_NUMBER varchar(100),
    ID_CARD_PHOTO_ID uuid,
    SELFIE_PHOTO_ID uuid,
    NPWP_NUMBER varchar(100),
    NPWP_DATE date,
    NPWP_PHOTO_ID uuid,
    KK_PHOTO_ID uuid,
    --
    primary key (ID)
);