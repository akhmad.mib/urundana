alter table URUNDANA_USER_PROFILE rename column married_status_id to married_status_id__u09455 ;
alter table URUNDANA_USER_PROFILE drop constraint FK_URUNDANA_USER_PROFILE_ON_MARRIED_STATUS ;
drop index IDX_URUNDANA_USER_PROFILE_ON_MARRIED_STATUS ;
alter table URUNDANA_USER_PROFILE rename column nationality_id to nationality_id__u52464 ;
alter table URUNDANA_USER_PROFILE drop constraint FK_URUNDANA_USER_PROFILE_ON_NATIONALITY ;
drop index IDX_URUNDANA_USER_PROFILE_ON_NATIONALITY ;
alter table URUNDANA_USER_PROFILE rename column gender_id to gender_id__u47659 ;
alter table URUNDANA_USER_PROFILE drop constraint FK_URUNDANA_USER_PROFILE_ON_GENDER ;
drop index IDX_URUNDANA_USER_PROFILE_ON_GENDER ;
alter table URUNDANA_USER_PROFILE rename column id to id__u08551 ;
alter table URUNDANA_USER_PROFILE alter column id__u08551 drop not null ;
alter table URUNDANA_USER_PROFILE add column ID varchar(255) ^
update URUNDANA_USER_PROFILE set ID = '' where ID is null ;
alter table URUNDANA_USER_PROFILE alter column ID set not null ;
alter table URUNDANA_USER_PROFILE add column GENDER_ID varchar(255) ;
alter table URUNDANA_USER_PROFILE add column NATIONALITY_ID varchar(255) ;
alter table URUNDANA_USER_PROFILE add column MARRIED_STATUS_ID varchar(255) ;
