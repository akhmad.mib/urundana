alter table URUNDANA_INVESTMENT_PREFERENCE_ROLE rename column id to id__u19544 ;
alter table URUNDANA_INVESTMENT_PREFERENCE_ROLE alter column id__u19544 drop not null ;
alter table URUNDANA_INVESTMENT_PREFERENCE_ROLE add column ID varchar(255) ^
update URUNDANA_INVESTMENT_PREFERENCE_ROLE set ID = '' where ID is null ;
alter table URUNDANA_INVESTMENT_PREFERENCE_ROLE alter column ID set not null ;
