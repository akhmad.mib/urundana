create table URUNDANA_NATIONALITY (
    ID bigint,
    --
    CODE varchar(100),
    LABEL varchar(255) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION_2ND text,
    --
    primary key (ID)
);