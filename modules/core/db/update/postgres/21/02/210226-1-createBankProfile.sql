create table URUNDANA_BANK_PROFILE (
    ID bigint,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    BANK_ID bigint,
    BRANCH_NAME varchar(100),
    OWNER_NAME varchar(100),
    ACCOUNT_NUMBER bigint,
    MOTHER_NAME varchar(100),
    HEIRS_NAME varchar(100),
    HEIR_RELATIONSHIP varchar(255),
    --
    primary key (ID)
);