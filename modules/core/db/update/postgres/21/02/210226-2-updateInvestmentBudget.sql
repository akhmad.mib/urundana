alter table URUNDANA_INVESTMENT_BUDGET rename column id to id__u27293 ;
alter table URUNDANA_INVESTMENT_BUDGET alter column id__u27293 drop not null ;
alter table URUNDANA_INVESTMENT_BUDGET add column ID varchar(255) ^
update URUNDANA_INVESTMENT_BUDGET set ID = '' where ID is null ;
alter table URUNDANA_INVESTMENT_BUDGET alter column ID set not null ;
