create table URUNDANA_JOB_PROFILE (
    ID bigint,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    LAST_EDUCATION_ID bigint,
    JOB_ID bigint,
    JOBINDUSTRY_ID bigint,
    INCOME_ID bigint,
    INCOME_SOURCE_ID bigint,
    BUSINESS_DESCRIPTION text,
    INCOME_SOURCE_DESCRIPTION text,
    --
    primary key (ID)
);