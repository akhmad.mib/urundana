-- begin URUNDANA_GENDER
create table URUNDANA_GENDER (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
)^
-- end URUNDANA_GENDER
-- begin URUNDANA_EDUCATION
create table URUNDANA_EDUCATION (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
)^
-- end URUNDANA_EDUCATION
-- begin URUNDANA_JOB
create table URUNDANA_JOB (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
)^
-- end URUNDANA_JOB

-- begin URUNDANA_INVESTMENT_BUDGET
create table URUNDANA_INVESTMENT_BUDGET (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
)^
-- end URUNDANA_INVESTMENT_BUDGET

-- begin URUNDANA_INCOME
create table URUNDANA_INCOME (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
)^
-- end URUNDANA_INCOME
-- begin URUNDANA_SOURCE_INFO
create table URUNDANA_SOURCE_INFO (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
)^
-- end URUNDANA_SOURCE_INFO
-- begin URUNDANA_INCOME_SOURCE
create table URUNDANA_INCOME_SOURCE (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
)^
-- end URUNDANA_INCOME_SOURCE
-- begin URUNDANA_NATIONALITY
create table URUNDANA_NATIONALITY (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
)^
-- end URUNDANA_NATIONALITY

-- begin URUNDANA_BANK
create table URUNDANA_BANK (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
)^
-- end URUNDANA_BANK

-- begin URUNDANA_USER_PROFILE
create table URUNDANA_USER_PROFILE (
    ID varchar(255),
    UUID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    --
    USER_ID uuid,
    PERSONAL_DATA_ID varchar(255),
    JOB_ID varchar(255),
    DOCUMENT_ID varchar(255),
    BANK_ID varchar(255),
    PREFERENCE_ID varchar(255),
    STEP integer,
    HAS_FINISH boolean,
    --
    primary key (ID)
)^
-- end URUNDANA_USER_PROFILE
-- begin URUNDANA_JOB_PROFILE
create table URUNDANA_JOB_PROFILE (
    ID varchar(255),
    UUID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    --
    USER_ID uuid,
    LAST_EDUCATION_ID varchar(255),
    JOB_ID varchar(255),
    INDUSTRY_CATEGORY_ID varchar(255),
    INCOME_ID varchar(255),
    INCOME_SOURCE_ID varchar(255),
    BUSINESS_DESCRIPTION text,
    INCOME_SOURCE_DESCRIPTION text,
    --
    primary key (ID)
)^
-- end URUNDANA_JOB_PROFILE
-- begin URUNDANA_BANK_PROFILE
create table URUNDANA_BANK_PROFILE (
    ID varchar(255),
    UUID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    --
    USER_ID uuid,
    BANK_ID varchar(255),
    BRANCH_NAME varchar(100),
    OWNER_NAME varchar(100),
    ACCOUNT_NUMBER bigint,
    MOTHER_NAME varchar(100),
    HEIRS_NAME varchar(100),
    HEIR_RELATIONSHIP varchar(255),
    --
    primary key (ID)
)^
-- end URUNDANA_BANK_PROFILE
-- begin URUNDANA_DOCUMENT_PROFILE
create table URUNDANA_DOCUMENT_PROFILE (
    ID varchar(255),
    UUID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    --
    USER_ID uuid,
    SID_NUMBER varchar(100),
    ID_CARD_NUMBER varchar(100),
    ID_CARD_PHOTO_ID uuid,
    SELFIE_PHOTO_ID uuid,
    NPWP_NUMBER varchar(100),
    NPWP_DATE date,
    NPWP_PHOTO_ID uuid,
    KK_PHOTO_ID uuid,
    --
    primary key (ID)
)^
-- end URUNDANA_DOCUMENT_PROFILE
-- begin URUNDANA_CITY
create table URUNDANA_CITY (
    ID integer,
    --
    PROVINCE_ID integer not null,
    PROVINCE_NAME varchar(100),
    NAME varchar(100) not null,
    TYPE_OF_AREA varchar(50),
    --
    primary key (ID)
)^
-- end URUNDANA_CITY
-- begin URUNDANA_SUB_DISTRICT
create table URUNDANA_SUB_DISTRICT (
    ID integer,
    --
    PROVINCE_ID integer,
    PROVINCE_NAME varchar(100),
    CITY_ID integer not null,
    CITY_NAME varchar(100),
    NAME varchar(100) not null,
    --
    primary key (ID)
)^
-- end URUNDANA_SUB_DISTRICT
-- begin URUNDANA_PROVINCE
create table URUNDANA_PROVINCE (
    ID integer,
    --
    NAME varchar(100) not null,
    --
    primary key (ID)
)^
-- end URUNDANA_PROVINCE
-- begin URUNDANA_PERSONAL_DATA_PROFILE
create table URUNDANA_PERSONAL_DATA_PROFILE (
    ID varchar(255),
    UUID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    --
    USER_ID uuid,
    FIRST_NAME varchar(50) not null,
    LAST_NAME varchar(50),
    GENDER_ID varchar(255),
    BIRTH_OF_PLACE varchar(100),
    BIRTH_OF_DATE date,
    NATIONALITY_ID varchar(255),
    MARITAL_STATUS_ID varchar(255),
    MOTHER_NAME varchar(100),
    PROFIL_BACKGROUND text,
    PHONE_NUMBER varchar(15),
    LANDLINE_NUMBER varchar(15),
    ID_CARD_ADDRESS text,
    ID_CARD_PROVINCE_ID integer,
    ID_CARD_CITY_ID integer,
    ID_CARD_SUB_DISTRICT_ID integer,
    ID_CARD_URBAN_VILLAGE varchar(100),
    ID_CARD_RT integer,
    ID_CARD_RW integer,
    HOME_ADDRESS text,
    HOME_PROVINCE_ID integer,
    HOME_CITY_ID integer,
    HOME_SUB_DISTRICT_ID integer,
    HOME_URBAN_VILLAGE varchar(100),
    HOME_RT integer,
    HOME_RW integer,
    --
    primary key (ID)
)^
-- end URUNDANA_PERSONAL_DATA_PROFILE
-- begin URUNDANA_CAPITAL_FUNDING_JOB_INDUSTRY
create table URUNDANA_CAPITAL_FUNDING_JOB_INDUSTRY (
    ID integer,
    --
    CAPITAL_FUNDING_ID varchar(255),
    INDUSTRY_CATEGORY_ID varchar(255),
    --
    primary key (ID)
)^
-- end URUNDANA_CAPITAL_FUNDING_JOB_INDUSTRY
-- begin URUNDANA_MARITAL_STATUS
create table URUNDANA_MARITAL_STATUS (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
)^
-- end URUNDANA_MARITAL_STATUS
-- begin URUNDANA_INDUSTRY_CATEGORY
create table URUNDANA_INDUSTRY_CATEGORY (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    CAPITAL_FUNDING_ID varchar(255),
    --
    primary key (ID)
)^
-- end URUNDANA_INDUSTRY_CATEGORY
-- begin URUNDANA_USER_INVESTMENT_OBJECTIVE
create table URUNDANA_USER_INVESTMENT_OBJECTIVE (
    ID varchar(255),
    UUID uuid,
    --
    USER_ID uuid,
    INVESTMENT_OBJECTIVE_ID varchar(255),
    PREFERENCE_PROFILE_ID varchar(255),
    PROFILE_ID varchar(255),
    --
    primary key (ID)
)^
-- end URUNDANA_USER_INVESTMENT_OBJECTIVE
-- begin URUNDANA_JOB_POSITION
create table URUNDANA_JOB_POSITION (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
)^
-- end URUNDANA_JOB_POSITION
-- begin URUNDANA_FORM_OF_BUSINESS
create table URUNDANA_FORM_OF_BUSINESS (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
)^
-- end URUNDANA_FORM_OF_BUSINESS
-- begin URUNDANA_INVESTMENT_OBJECTIVE
create table URUNDANA_INVESTMENT_OBJECTIVE (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
)^
-- end URUNDANA_INVESTMENT_OBJECTIVE
-- begin URUNDANA_MARKETING_FUNNEL
create table URUNDANA_MARKETING_FUNNEL (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
)^
-- end URUNDANA_MARKETING_FUNNEL
-- begin URUNDANA_MANY_BRANCH_OF_BUSINESS
create table URUNDANA_MANY_BRANCH_OF_BUSINESS (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
)^
-- end URUNDANA_MANY_BRANCH_OF_BUSINESS
-- begin URUNDANA_INVESTMENT_RISK_PREFERENCE
create table URUNDANA_INVESTMENT_RISK_PREFERENCE (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
)^
-- end URUNDANA_INVESTMENT_RISK_PREFERENCE
-- begin URUNDANA_INVESTMENT_ROLE_PREFERENCE
create table URUNDANA_INVESTMENT_ROLE_PREFERENCE (
    ID varchar(255),
    UUID uuid,
    CODE varchar(100),
    LABEL varchar(100) not null,
    LABEL2ND varchar(100),
    DESCRIPTION text,
    DESCRIPTION2ND text,
    --
    primary key (ID)
)^
-- end URUNDANA_INVESTMENT_ROLE_PREFERENCE
-- begin URUNDANA_USER_INVESTMENT_PREFERENCE
create table URUNDANA_USER_INVESTMENT_PREFERENCE (
    ID varchar(255),
    UUID uuid,
    --
    USER_ID uuid,
    INVESTMENT_PREFERENCE_ID varchar(255),
    PREFERENCE_PROFILE_ID varchar(255),
    PROFILE_ID varchar(255),
    --
    primary key (ID)
)^
-- end URUNDANA_USER_INVESTMENT_PREFERENCE
-- begin URUNDANA_PREFERENCE_PROFILE
create table URUNDANA_PREFERENCE_PROFILE (
    ID varchar(255),
    UUID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    --
    USER_ID uuid,
    INVESTMENT_BUDGET_ID varchar(255),
    INVESTMENT_RISK_PREFERENCE_ID varchar(255),
    INVESTMENT_ROLE_PREFERENCE_ID varchar(255),
    SOURCE_INFO_ID varchar(255),
    --
    primary key (ID)
)^
-- end URUNDANA_PREFERENCE_PROFILE
-- begin URUNDANA_PROFILE
create table URUNDANA_PROFILE (
    ID varchar(255),
    UUID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    --
    USER_ID uuid,
    FIRST_NAME varchar(50) not null,
    LAST_NAME varchar(50),
    GENDER_ID varchar(255),
    BIRTH_OF_PLACE varchar(100),
    BIRTH_OF_DATE date,
    NATIONALITY_ID varchar(255),
    MARITAL_STATUS_ID varchar(255),
    PROFIL_BACKGROUND text,
    PHONE_NUMBER varchar(15),
    LANDLINE_NUMBER varchar(15),
    ID_CARD_ADDRESS text,
    ID_CARD_PROVINCE_ID integer,
    ID_CARD_CITY_ID integer,
    ID_CARD_SUB_DISTRICT_ID integer,
    ID_CARD_URBAN_VILLAGE varchar(100),
    ID_CARD_RT integer,
    ID_CARD_RW integer,
    HOME_ADDRESS text,
    HOME_PROVINCE_ID integer,
    HOME_CITY_ID integer,
    HOME_SUB_DISTRICT_ID integer,
    HOME_URBAN_VILLAGE varchar(100),
    HOME_RT integer,
    HOME_RW integer,
    LAST_EDUCATION_ID varchar(255),
    JOB_ID varchar(255),
    INDUSTRY_CATEGORY_ID varchar(255),
    INCOME_ID varchar(255),
    INCOME_SOURCE_ID varchar(255),
    BUSINESS_DESCRIPTION text,
    INCOME_SOURCE_DESCRIPTION text,
    SID_NUMBER varchar(100),
    ID_CARD_NUMBER varchar(100),
    ID_CARD_PHOTO_ID uuid,
    SELFIE_PHOTO_ID uuid,
    NPWP_NUMBER varchar(100),
    NPWP_DATE date,
    NPWP_PHOTO_ID uuid,
    KK_PHOTO_ID uuid,
    BANK_ID varchar(255),
    BRANCH_NAME varchar(100),
    OWNER_NAME varchar(100),
    ACCOUNT_NUMBER bigint,
    MOTHER_NAME varchar(100),
    HEIRS_NAME varchar(100),
    HEIR_RELATIONSHIP varchar(255),
    INVESTMENT_BUDGET_ID varchar(255),
    INVESTMENT_RISK_PREFERENCE_ID varchar(255),
    INVESTMENT_ROLE_PREFERENCE_ID varchar(255),
    SOURCE_INFO_ID varchar(255),
    --
    primary key (ID)
)^
-- end URUNDANA_PROFILE
-- begin URUNDANA_CAPITAL_FUNDING
create table URUNDANA_CAPITAL_FUNDING (
    ID varchar(255),
    UUID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    --
    FULL_NAME varchar(100),
    JOB_POSITION_ID varchar(255),
    JOB_POSITION_OTHERS varchar(255),
    EMAIL varchar(150),
    WHATSAPP_NUMBER varchar(15),
    BUSINESS_BRAND varchar(150),
    INDUSTRY_CATEGORY_OTHERS varchar(150),
    PRODUCT_OFFERED varchar(255),
    FORM_OF_BUSINESS_ID varchar(255),
    FORM_OF_BUSINESS_OTHERS varchar(255),
    YEAR_OF_BUSINESS integer,
    MANY_BRANCH_OF_BUSINESS_ID varchar(255),
    MANY_BRANCH_OF_BUSINESS_OTHERS varchar(150),
    BUSINESS_DESCRIPTION text,
    MARKETING_FUNNEL_ID varchar(255),
    OMZET_PER_MONTH bigint,
    PROFIT_PER_MONTH bigint,
    BUSINESS_PLAN text,
    AMOUNT_OF_FUNDING bigint,
    OPERATIONAL_PLAN text,
    SOURCE_INFO_ID varchar(255),
    APPROVAL_STATUS integer,
    APPROVAL_ACTION_TIME timestamp,
    APPROVAL_ACTION_BY_ID uuid,
    CAPITAL_FUNDING_PROGRESS_STATUS integer,
    PRE_ORDER_DATE date,
    STOCK_OFFERING_DATE date,
    FUNDING_FULFILLED date,
    SUBMISSION_OF_FUNDS_DATE date,
    DIVIDEND_DISTRIBUTION_DATE date,
    TARGET_FUNDING_FULFILLED_DATE date,
    --
    primary key (ID)
)^
-- end URUNDANA_CAPITAL_FUNDING
-- begin URUNDANA_URBAN_VILLAGE
create table URUNDANA_URBAN_VILLAGE (
    ID integer,
    --
    PROVINCE_ID integer,
    PROVINCE_NAME varchar(100),
    CITY_ID integer,
    CITY_NAME varchar(100),
    SUB_DISTRICT_ID integer,
    SUB_DISTRICT_NAME varchar(100),
    NAME varchar(100) not null,
    TYPE_OF_AREA varchar(50),
    --
    primary key (ID)
)^
-- end URUNDANA_URBAN_VILLAGE
