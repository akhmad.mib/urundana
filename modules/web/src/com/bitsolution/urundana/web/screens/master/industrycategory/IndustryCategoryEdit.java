package com.bitsolution.urundana.web.screens.master.industrycategory;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.IndustryCategory;

@UiController("urundana_IndustryCategory.edit")
@UiDescriptor("industry-category-edit.xml")
@EditedEntityContainer("industryCategoryDc")
@LoadDataBeforeShow
public class IndustryCategoryEdit extends StandardEditor<IndustryCategory> {
}