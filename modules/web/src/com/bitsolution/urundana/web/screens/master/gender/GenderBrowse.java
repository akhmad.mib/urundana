package com.bitsolution.urundana.web.screens.master.gender;

import com.haulmont.cuba.gui.Route;
import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.Gender;

@Route("gender")
@UiController("urundana_Gender.browse")
@UiDescriptor("gender-browse.xml")
@LookupComponent("gendersTable")
@LoadDataBeforeShow
public class GenderBrowse extends StandardLookup<Gender> {
}