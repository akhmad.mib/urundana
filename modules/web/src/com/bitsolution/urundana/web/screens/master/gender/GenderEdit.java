package com.bitsolution.urundana.web.screens.master.gender;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.Gender;

@UiController("urundana_Gender.edit")
@UiDescriptor("gender-edit.xml")
@EditedEntityContainer("genderDc")
@LoadDataBeforeShow
public class GenderEdit extends StandardEditor<Gender> {
}