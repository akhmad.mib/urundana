package com.bitsolution.urundana.web.screens.master.subdistrict;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.SubDistrict;

@UiController("urundana_SubDistrict.browse")
@UiDescriptor("sub-district-browse.xml")
@LookupComponent("subDistrictsTable")
@LoadDataBeforeShow
public class SubDistrictBrowse extends StandardLookup<SubDistrict> {
}