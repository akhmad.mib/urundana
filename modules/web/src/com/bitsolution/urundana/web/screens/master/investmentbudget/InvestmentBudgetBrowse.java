package com.bitsolution.urundana.web.screens.master.investmentbudget;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.InvestmentBudget;

@UiController("urundana_InvestmentBudget.browse")
@UiDescriptor("investment-budget-browse.xml")
@LookupComponent("investmentBudgetsTable")
@LoadDataBeforeShow
public class InvestmentBudgetBrowse extends StandardLookup<InvestmentBudget> {
}