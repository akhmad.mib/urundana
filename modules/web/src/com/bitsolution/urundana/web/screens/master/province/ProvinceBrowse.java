package com.bitsolution.urundana.web.screens.master.province;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.Province;

@UiController("urundana_Province.browse")
@UiDescriptor("province-browse.xml")
@LookupComponent("provincesTable")
@LoadDataBeforeShow
public class ProvinceBrowse extends StandardLookup<Province> {
}