package com.bitsolution.urundana.web.screens.master.job;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.Job;

@UiController("urundana_Job.browse")
@UiDescriptor("job-browse.xml")
@LookupComponent("jobsTable")
@LoadDataBeforeShow
public class JobBrowse extends StandardLookup<Job> {
}