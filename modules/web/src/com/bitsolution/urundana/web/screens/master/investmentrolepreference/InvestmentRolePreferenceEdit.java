package com.bitsolution.urundana.web.screens.master.investmentrolepreference;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.InvestmentRolePreference;

@UiController("urundana_InvestmentRolePreference.edit")
@UiDescriptor("investment-role-preference-edit.xml")
@EditedEntityContainer("investmentRolePreferenceDc")
@LoadDataBeforeShow
public class InvestmentRolePreferenceEdit extends StandardEditor<InvestmentRolePreference> {
}