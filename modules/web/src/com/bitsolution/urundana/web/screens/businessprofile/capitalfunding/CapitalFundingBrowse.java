package com.bitsolution.urundana.web.screens.businessprofile.capitalfunding;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.businessprofile.CapitalFunding;

@UiController("urundana_CapitalFunding.browse")
@UiDescriptor("capital-funding-browse.xml")
@LookupComponent("capitalFundingsTable")
@LoadDataBeforeShow
public class CapitalFundingBrowse extends StandardLookup<CapitalFunding> {
}