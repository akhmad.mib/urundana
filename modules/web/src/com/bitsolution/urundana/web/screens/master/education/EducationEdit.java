package com.bitsolution.urundana.web.screens.master.education;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.Education;

@UiController("urundana_Education.edit")
@UiDescriptor("education-edit.xml")
@EditedEntityContainer("educationDc")
@LoadDataBeforeShow
public class EducationEdit extends StandardEditor<Education> {
}