package com.bitsolution.urundana.web.screens.master.city;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.City;

@UiController("urundana_City.browse")
@UiDescriptor("city-browse.xml")
@LookupComponent("citiesTable")
@LoadDataBeforeShow
public class CityBrowse extends StandardLookup<City> {
}