package com.bitsolution.urundana.web.screens.userprofile;

import com.bitsolution.urundana.entity.userprofile.PersonalDataProfile;
import com.bitsolution.urundana.web.screens.userprofile.personaldataprofile.PersonalDataProfileEdit;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.userprofile.UserProfile;

import javax.inject.Inject;

@UiController("urundana_UserProfile.edit")
@UiDescriptor("user-profile-edit.xml")
@EditedEntityContainer("userProfileDc")
@LoadDataBeforeShow
public class UserProfileEdit extends StandardEditor<UserProfile> {

    @Inject
    private Notifications notifications;

    @Inject
    private PersonalDataProfileEdit personalDataFragment;

    @Inject
    private InstanceContainer<UserProfile> userProfileDc;

    @Subscribe("testParentBtn")
    public void onTestParentBtnClick(Button.ClickEvent event) {
        PersonalDataProfile personalDataProfile = personalDataFragment.getPersonalDataProfile();
        notifications.create().withCaption("child! : "+personalDataProfile.getFirstName()+" | parent! : "+userProfileDc.getItem().getPersonalData().getFirstName()).show();
    }

}