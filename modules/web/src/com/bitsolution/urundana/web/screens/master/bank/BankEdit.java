package com.bitsolution.urundana.web.screens.master.bank;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.Bank;

@UiController("urundana_Bank.edit")
@UiDescriptor("bank-edit.xml")
@EditedEntityContainer("bankDc")
@LoadDataBeforeShow
public class BankEdit extends StandardEditor<Bank> {
}