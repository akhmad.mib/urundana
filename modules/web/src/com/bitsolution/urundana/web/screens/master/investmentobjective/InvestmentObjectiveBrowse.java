package com.bitsolution.urundana.web.screens.master.investmentobjective;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.InvestmentObjective;

@UiController("urundana_InvestmentObjective.browse")
@UiDescriptor("investment-objective-browse.xml")
@LookupComponent("investmentObjectivesTable")
@LoadDataBeforeShow
public class InvestmentObjectiveBrowse extends StandardLookup<InvestmentObjective> {
}