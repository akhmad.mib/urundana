package com.bitsolution.urundana.web.screens.master.investmentriskpreference;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.InvestmentRiskPreference;

@UiController("urundana_InvestmentRiskPreference.browse")
@UiDescriptor("investment-risk-preference-browse.xml")
@LookupComponent("investmentRiskPreferencesTable")
@LoadDataBeforeShow
public class InvestmentRiskPreferenceBrowse extends StandardLookup<InvestmentRiskPreference> {
}