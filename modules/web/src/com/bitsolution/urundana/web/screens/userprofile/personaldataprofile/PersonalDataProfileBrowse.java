package com.bitsolution.urundana.web.screens.userprofile.personaldataprofile;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.userprofile.PersonalDataProfile;

@UiController("urundana_PersonalDataProfile.browse")
@UiDescriptor("personal-data-profile-browse.xml")
@LookupComponent("personalDataProfilesTable")
@LoadDataBeforeShow
public class PersonalDataProfileBrowse extends StandardLookup<PersonalDataProfile> {
}