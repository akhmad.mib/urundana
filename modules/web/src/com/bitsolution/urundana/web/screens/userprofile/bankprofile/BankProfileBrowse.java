package com.bitsolution.urundana.web.screens.userprofile.bankprofile;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.userprofile.BankProfile;

@UiController("urundana_BankProfile.browse")
@UiDescriptor("bank-profile-browse.xml")
@LookupComponent("bankProfilesTable")
@LoadDataBeforeShow
public class BankProfileBrowse extends StandardLookup<BankProfile> {
}