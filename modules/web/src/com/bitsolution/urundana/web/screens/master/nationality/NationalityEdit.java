package com.bitsolution.urundana.web.screens.master.nationality;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.Nationality;

@UiController("urundana_Nationality.edit")
@UiDescriptor("nationality-edit.xml")
@EditedEntityContainer("nationalityDc")
@LoadDataBeforeShow
public class NationalityEdit extends StandardEditor<Nationality> {
}