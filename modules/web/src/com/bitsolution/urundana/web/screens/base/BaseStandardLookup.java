package com.bitsolution.urundana.web.screens.base;

import com.bitsolution.urundana.web.screens.master.gender.GenderBrowse;
import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.UrlRouting;
import com.haulmont.cuba.gui.screen.StandardLookup;
import com.haulmont.cuba.gui.screen.Subscribe;

import javax.inject.Inject;

public class BaseStandardLookup<T extends Entity> extends StandardLookup<T> {
    @Inject
    private Screens screens;

    @Inject
    private UrlRouting urlRouting;

    @Subscribe
    public void onInit(InitEvent event) {
//        GenderBrowse screen = screens.create(GenderBrowse.class);
//        screen.show();
        super.initActions(event);
//        urlRouting.replaceState(screen);

    }

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
//        closeWithDefaultAction();
    }
}
