package com.bitsolution.urundana.web.screens.userprofile.documentprofile;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.userprofile.DocumentProfile;

@UiController("urundana_DocumentProfile.browse")
@UiDescriptor("document-profile-browse.xml")
@LookupComponent("documentProfilesTable")
@LoadDataBeforeShow
public class DocumentProfileBrowse extends StandardLookup<DocumentProfile> {
}