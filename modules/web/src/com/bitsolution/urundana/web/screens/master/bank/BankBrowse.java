package com.bitsolution.urundana.web.screens.master.bank;

import com.bitsolution.urundana.web.screens.base.BaseStandardLookup;
import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.Bank;

@UiController("urundana_Bank.browse")
@UiDescriptor("bank-browse.xml")
@LookupComponent("banksTable")
@LoadDataBeforeShow
public class BankBrowse extends BaseStandardLookup<Bank> {


}