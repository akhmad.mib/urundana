package com.bitsolution.urundana.web.screens.master.investmentobjective;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.InvestmentObjective;

@UiController("urundana_InvestmentObjective.edit")
@UiDescriptor("investment-objective-edit.xml")
@EditedEntityContainer("investmentObjectiveDc")
@LoadDataBeforeShow
public class InvestmentObjectiveEdit extends StandardEditor<InvestmentObjective> {
}