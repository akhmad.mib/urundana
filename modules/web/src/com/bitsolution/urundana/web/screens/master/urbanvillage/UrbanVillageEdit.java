package com.bitsolution.urundana.web.screens.master.urbanvillage;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.UrbanVillage;

@UiController("urundana_UrbanVillage.edit")
@UiDescriptor("urban-village-edit.xml")
@EditedEntityContainer("urbanVillageDc")
@LoadDataBeforeShow
public class UrbanVillageEdit extends StandardEditor<UrbanVillage> {
}