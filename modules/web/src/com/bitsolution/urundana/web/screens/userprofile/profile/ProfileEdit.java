package com.bitsolution.urundana.web.screens.userprofile.profile;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.userprofile.Profile;

@UiController("urundana_Profile.edit")
@UiDescriptor("profile-edit.xml")
@EditedEntityContainer("profileDc")
@LoadDataBeforeShow
public class ProfileEdit extends StandardEditor<Profile> {
}