package com.bitsolution.urundana.web.screens.master.job;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.Job;

@UiController("urundana_Job.edit")
@UiDescriptor("job-edit.xml")
@EditedEntityContainer("jobDc")
@LoadDataBeforeShow
public class JobEdit extends StandardEditor<Job> {
}