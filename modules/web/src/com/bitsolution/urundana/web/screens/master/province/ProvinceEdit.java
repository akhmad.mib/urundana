package com.bitsolution.urundana.web.screens.master.province;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.Province;

@UiController("urundana_Province.edit")
@UiDescriptor("province-edit.xml")
@EditedEntityContainer("provinceDc")
@LoadDataBeforeShow
public class ProvinceEdit extends StandardEditor<Province> {
}