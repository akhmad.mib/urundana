package com.bitsolution.urundana.web.screens.master.investmentbudget;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.InvestmentBudget;

@UiController("urundana_InvestmentBudget.edit")
@UiDescriptor("investment-budget-edit.xml")
@EditedEntityContainer("investmentBudgetDc")
@LoadDataBeforeShow
public class InvestmentBudgetEdit extends StandardEditor<InvestmentBudget> {
}