package com.bitsolution.urundana.web.screens.userprofile.jobprofile;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.userprofile.JobProfile;

@UiController("urundana_JobProfile.browse")
@UiDescriptor("job-profile-browse.xml")
@LookupComponent("jobProfilesTable")
@LoadDataBeforeShow
public class JobProfileBrowse extends StandardLookup<JobProfile> {
}