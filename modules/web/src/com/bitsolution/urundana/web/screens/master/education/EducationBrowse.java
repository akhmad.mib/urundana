package com.bitsolution.urundana.web.screens.master.education;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.Education;

@UiController("urundana_Education.browse")
@UiDescriptor("education-browse.xml")
@LookupComponent("educationsTable")
@LoadDataBeforeShow
public class EducationBrowse extends StandardLookup<Education> {
}