package com.bitsolution.urundana.web.screens.userprofile.profile;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.userprofile.Profile;

@UiController("urundana_Profile.browse")
@UiDescriptor("profile-browse.xml")
@LookupComponent("profilesTable")
@LoadDataBeforeShow
public class ProfileBrowse extends StandardLookup<Profile> {
}