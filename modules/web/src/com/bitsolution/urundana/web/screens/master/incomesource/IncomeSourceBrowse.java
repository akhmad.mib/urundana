package com.bitsolution.urundana.web.screens.master.incomesource;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.IncomeSource;

@UiController("urundana_IncomeSource.browse")
@UiDescriptor("income-source-browse.xml")
@LookupComponent("incomeSourcesTable")
@LoadDataBeforeShow
public class IncomeSourceBrowse extends StandardLookup<IncomeSource> {
}