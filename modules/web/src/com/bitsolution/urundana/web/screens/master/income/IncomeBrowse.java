package com.bitsolution.urundana.web.screens.master.income;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.Income;

@UiController("urundana_Income.browse")
@UiDescriptor("income-browse.xml")
@LookupComponent("incomesTable")
@LoadDataBeforeShow
public class IncomeBrowse extends StandardLookup<Income> {
}