package com.bitsolution.urundana.web.screens.master.investmentriskpreference;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.InvestmentRiskPreference;

@UiController("urundana_InvestmentRiskPreference.edit")
@UiDescriptor("investment-risk-preference-edit.xml")
@EditedEntityContainer("investmentRiskPreferenceDc")
@LoadDataBeforeShow
public class InvestmentRiskPreferenceEdit extends StandardEditor<InvestmentRiskPreference> {
}