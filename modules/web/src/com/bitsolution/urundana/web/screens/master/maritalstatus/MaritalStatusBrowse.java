package com.bitsolution.urundana.web.screens.master.maritalstatus;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.MaritalStatus;

@UiController("urundana_MaritalStatus.browse")
@UiDescriptor("marital-status-browse.xml")
@LookupComponent("maritalStatusesTable")
@LoadDataBeforeShow
public class MaritalStatusBrowse extends StandardLookup<MaritalStatus> {
}