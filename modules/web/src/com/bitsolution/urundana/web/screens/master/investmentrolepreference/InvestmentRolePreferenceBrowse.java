package com.bitsolution.urundana.web.screens.master.investmentrolepreference;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.InvestmentRolePreference;

@UiController("urundana_InvestmentRolePreference.browse")
@UiDescriptor("investment-role-preference-browse.xml")
@LookupComponent("investmentRolePreferencesTable")
@LoadDataBeforeShow
public class InvestmentRolePreferenceBrowse extends StandardLookup<InvestmentRolePreference> {
}