package com.bitsolution.urundana.web.screens.master.maritalstatus;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.MaritalStatus;

@UiController("urundana_MaritalStatus.edit")
@UiDescriptor("marital-status-edit.xml")
@EditedEntityContainer("maritalStatusDc")
@LoadDataBeforeShow
public class MaritalStatusEdit extends StandardEditor<MaritalStatus> {
}