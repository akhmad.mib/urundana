package com.bitsolution.urundana.web.screens.master.subdistrict;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.SubDistrict;

@UiController("urundana_SubDistrict.edit")
@UiDescriptor("sub-district-edit.xml")
@EditedEntityContainer("subDistrictDc")
@LoadDataBeforeShow
public class SubDistrictEdit extends StandardEditor<SubDistrict> {
}