package com.bitsolution.urundana.web.screens.master.industrycategory;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.IndustryCategory;

@UiController("urundana_IndustryCategory.browse")
@UiDescriptor("industry-category-browse.xml")
@LookupComponent("industryCategoriesTable")
@LoadDataBeforeShow
public class IndustryCategoryBrowse extends StandardLookup<IndustryCategory> {
}