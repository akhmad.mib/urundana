package com.bitsolution.urundana.web.screens.userprofile.documentprofile;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.userprofile.DocumentProfile;

@UiController("urundana_DocumentProfile.edit")
@UiDescriptor("document-profile-edit.xml")
@EditedEntityContainer("documentProfileDc")
@LoadDataBeforeShow
public class DocumentProfileEdit extends ScreenFragment {
}