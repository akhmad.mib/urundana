package com.bitsolution.urundana.web.screens.master.incomesource;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.IncomeSource;

@UiController("urundana_IncomeSource.edit")
@UiDescriptor("income-source-edit.xml")
@EditedEntityContainer("incomeSourceDc")
@LoadDataBeforeShow
public class IncomeSourceEdit extends StandardEditor<IncomeSource> {
}