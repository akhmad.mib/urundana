package com.bitsolution.urundana.web.screens.master.city;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.City;

@UiController("urundana_City.edit")
@UiDescriptor("city-edit.xml")
@EditedEntityContainer("cityDc")
@LoadDataBeforeShow
public class CityEdit extends StandardEditor<City> {
}