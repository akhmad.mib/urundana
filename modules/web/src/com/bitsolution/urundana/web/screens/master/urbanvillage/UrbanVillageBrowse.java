package com.bitsolution.urundana.web.screens.master.urbanvillage;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.UrbanVillage;

@UiController("urundana_UrbanVillage.browse")
@UiDescriptor("urban-village-browse.xml")
@LookupComponent("urbanVillagesTable")
@LoadDataBeforeShow
public class UrbanVillageBrowse extends StandardLookup<UrbanVillage> {
}