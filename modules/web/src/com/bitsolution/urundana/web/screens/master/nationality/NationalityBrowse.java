package com.bitsolution.urundana.web.screens.master.nationality;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.Nationality;

@UiController("urundana_Nationality.browse")
@UiDescriptor("nationality-browse.xml")
@LookupComponent("nationalitiesTable")
@LoadDataBeforeShow
public class NationalityBrowse extends StandardLookup<Nationality> {
}