package com.bitsolution.urundana.web.screens.master.sourceinfo;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.SourceInfo;

@UiController("urundana_SourceInfo.browse")
@UiDescriptor("source-info-browse.xml")
@LookupComponent("sourceInfoesTable")
@LoadDataBeforeShow
public class SourceInfoBrowse extends StandardLookup<SourceInfo> {
}