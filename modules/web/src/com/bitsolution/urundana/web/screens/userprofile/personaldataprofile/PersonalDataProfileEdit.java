package com.bitsolution.urundana.web.screens.userprofile.personaldataprofile;

import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.userprofile.PersonalDataProfile;

import javax.inject.Inject;

@UiController("urundana_PersonalDataProfile.edit")
@UiDescriptor("personal-data-profile-edit.xml")
@EditedEntityContainer("personalDataProfileDc")
@LoadDataBeforeShow
public class PersonalDataProfileEdit extends ScreenFragment {

    @Inject
    InstanceContainer<PersonalDataProfile> personalDataProfileDc;

    @Inject
    private Notifications notifications;

    @Subscribe(target = Target.PARENT_CONTROLLER)
    public void onInit(Screen.InitEvent event) {

    }

    @Subscribe(target = Target.PARENT_CONTROLLER)
    public void onAfterShow(Screen.AfterShowEvent event) {
        PersonalDataProfile personalDataProfile = personalDataProfileDc.getItem();
        notifications.create().withCaption("PersonalDataProfileEdit! : "+personalDataProfile.getFirstName()).show();
    }

    @Subscribe("testBtn")
    public void onTestBtnClick(Button.ClickEvent event) {
        PersonalDataProfile personalDataProfile = personalDataProfileDc.getItem();
        notifications.create().withCaption("PersonalDataProfileEdit! : "+personalDataProfile.getFirstName()).show();
    }

    public PersonalDataProfile getPersonalDataProfile(){
        return personalDataProfileDc.getItem();
    }
}