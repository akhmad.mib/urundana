package com.bitsolution.urundana.web.screens.userprofile.jobprofile;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.userprofile.JobProfile;

@UiController("urundana_JobProfile.edit")
@UiDescriptor("job-profile-edit.xml")
@EditedEntityContainer("jobProfileDc")
@LoadDataBeforeShow
public class JobProfileEdit extends ScreenFragment {
}