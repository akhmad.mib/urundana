package com.bitsolution.urundana.web.screens.userprofile;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.userprofile.UserProfile;

@UiController("urundana_UserProfile.browse")
@UiDescriptor("user-profile-browse.xml")
@LookupComponent("userProfilesTable")
@LoadDataBeforeShow
public class UserProfileBrowse extends StandardLookup<UserProfile> {
}