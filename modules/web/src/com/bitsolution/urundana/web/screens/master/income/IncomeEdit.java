package com.bitsolution.urundana.web.screens.master.income;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.Income;

@UiController("urundana_Income.edit")
@UiDescriptor("income-edit.xml")
@EditedEntityContainer("incomeDc")
@LoadDataBeforeShow
public class IncomeEdit extends StandardEditor<Income> {
}