package com.bitsolution.urundana.web.screens.businessprofile.capitalfunding;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.businessprofile.CapitalFunding;

@UiController("urundana_CapitalFunding.edit")
@UiDescriptor("capital-funding-edit.xml")
@EditedEntityContainer("capitalFundingDc")
@LoadDataBeforeShow
public class CapitalFundingEdit extends StandardEditor<CapitalFunding> {
}