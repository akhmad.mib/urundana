package com.bitsolution.urundana.web.screens.master.sourceinfo;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.SourceInfo;

@UiController("urundana_SourceInfo.edit")
@UiDescriptor("source-info-edit.xml")
@EditedEntityContainer("sourceInfoDc")
@LoadDataBeforeShow
public class SourceInfoEdit extends StandardEditor<SourceInfo> {
}