package com.bitsolution.urundana.web.screens.businessprofile;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.master.Bank;

@UiController("urundana_BusinessList.browse")
@UiDescriptor("business-list-browse.xml")
@LookupComponent("banksTable")
@LoadDataBeforeShow
public class BusinessListBrowse extends StandardLookup<Bank> {
}