package com.bitsolution.urundana.web.screens.userprofile.bankprofile;

import com.haulmont.cuba.gui.screen.*;
import com.bitsolution.urundana.entity.userprofile.BankProfile;

@UiController("urundana_BankProfile.edit")
@UiDescriptor("bank-profile-edit.xml")
@EditedEntityContainer("bankProfileDc")
@LoadDataBeforeShow
public class BankProfileEdit extends ScreenFragment {
}