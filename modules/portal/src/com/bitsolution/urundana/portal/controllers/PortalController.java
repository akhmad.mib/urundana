package com.bitsolution.urundana.portal.controllers;

import com.bitsolution.urundana.entity.master.Bank;
import com.bitsolution.urundana.portal.controllers.base.BaseController;
import com.haulmont.cuba.core.app.DataService;
import com.haulmont.cuba.core.app.importexport.EntityImportExportService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.portal.security.PortalSessionProvider;
import com.haulmont.cuba.security.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;


@Controller
public class PortalController extends BaseController {

    @Inject
    protected DataService dataService;

    @Inject
    private EntityImportExportService entityImportExportService;

    @Inject
    protected DataManager dataManager;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        initModel(model);
        if (PortalSessionProvider.getUserSession().isAuthenticated()) {
            LoadContext l = new LoadContext(User.class);
            l.setQueryString("select u from sec$User u");
            model.addAttribute("users", dataService.loadList(l));
        }
        return "cuba/index";
    }

    @RequestMapping(value = "/test/api/public/bank", method = RequestMethod.GET)
    public String index2(Model model) {
        LoadContext<Bank> loadContext = LoadContext.create(Bank.class)
                .setQuery(
                        LoadContext.createQuery("select u from urundana_Bank u")
                )
                .setView("_local");
        List<Bank> list = dataManager.loadList(loadContext);
        System.out.println(list.size());
        String jsonFromDs = entityImportExportService.exportEntitiesToJSON(list);
        return jsonFromDs;
    }

    @RequestMapping(value = "/username", method = RequestMethod.GET)
    @ResponseBody
    public String currentUserNameSimple(HttpServletRequest request) {
        Principal principal = request.getUserPrincipal();
        return principal.getName();
    }
}
