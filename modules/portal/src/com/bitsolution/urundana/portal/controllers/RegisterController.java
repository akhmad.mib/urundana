package com.bitsolution.urundana.portal.controllers;

import com.bitsolution.urundana.dto.userprofile.RegisterUserDto;
import com.bitsolution.urundana.portal.controllers.base.BaseController;
import com.bitsolution.urundana.result.userprofile.RegisterUserActionResult;
import com.bitsolution.urundana.service.userprofile.BankProfileService;
import com.bitsolution.urundana.service.userprofile.RegisterService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;

@Controller
public class RegisterController extends BaseController {

    @Inject
    BankProfileService bankProfileService;

    @Inject
    RegisterService registerService;
    
    @GetMapping(value = "register")
    public String register(Model model) {
        RegisterUserDto registerUserDto = new RegisterUserDto();
        model.addAttribute("registerUser",registerUserDto);

        return "cuba/register";
    }

    @PostMapping(value = "register")
    public String registerPost(@Valid RegisterUserDto registerUserDto, Model model, BindingResult bindingResult) {
        RegisterUserActionResult actionResult = registerService.registerUser(registerUserDto);
        if(actionResult.getSuccess()){
            return "cuba/register_success";
        }else {
            model.addAttribute("registerUser", registerUserDto);
            return "cuba/register";
        }

    }

    @GetMapping(value = "register_success")
    public String registerSuccess(Model model) {

        return "cuba/register_success";
    }

    @GetMapping(value = "register_test")
    @ResponseBody
    public String registerTest() {
        return toJson(bankProfileService.findByUserId(getUser().getId()));
    }
}
