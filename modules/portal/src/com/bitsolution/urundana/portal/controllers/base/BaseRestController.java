package com.bitsolution.urundana.portal.controllers.base;

import com.bitsolution.urundana.service.SessionService;
import com.haulmont.cuba.core.app.importexport.EntityImportExportService;
import com.haulmont.cuba.core.app.serialization.EntitySerializationAPI;
import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.security.entity.User;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;

@RestController
public class BaseRestController {
    @Inject
    private UserSessionSource userSessionSource;

    @Inject
    private EntityImportExportService entityImportExportService;


    @Inject
    protected EntitySerializationAPI entitySerialization;

    @Inject
    SessionService sessionService;

    protected boolean isAnonymous(){
        return sessionService.isAnonymous(userSessionSource.getUserSession());
    }

    protected User getUser(){
        return userSessionSource.getUserSession().getUser();
    }

    protected Collection<String> getRoles(){
        return userSessionSource.getUserSession().getRoles();
    }

    protected String toListJson(Collection<? extends Entity> list){
        return entitySerialization.toJson(list);
    }

    protected String toJson(Entity object){
        Collection<Entity> list = new ArrayList<>();
        list.add(object);
        return entitySerialization.toJson(list);
    }

    protected void initModel(Model model){
        model.addAttribute("userAuth",getUser());
    }
}
