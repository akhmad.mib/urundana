package com.bitsolution.urundana.portal.controllers.base;

import com.bitsolution.urundana.entity.master.City;
import com.bitsolution.urundana.entity.master.Province;
import com.bitsolution.urundana.entity.master.SubDistrict;
import com.bitsolution.urundana.service.MasterRepositoryService;
import com.bitsolution.urundana.service.master.*;
import org.springframework.ui.Model;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class BaseMasterController extends BaseController{

    @Inject
    MasterRepositoryService masterRepositoryService;

    @Inject
    CityService cityService;

    @Inject
    SubDistrictService subDistrictService;


    protected void initAllMaster(Model model){
        initMasterBank(model);
        initMasterEducation(model);
        initMasterFormOfBusiness(model);
        initMasterGender(model);
        initMasterIncome(model);
        initMasterIncomeSource(model);
        initMasterIndustryCategory(model);
        initMasterInvestmentBudget(model);
        initMasterInvestmentObjective(model);
        initMasterInvestmentRiskPreference(model);
        initMasterInvestmentRolePreference(model);
        initMasterJobPosition(model);
        initMasterJob(model);
        initMasterManyBranchOfBusiness(model);
        initMasterMaritalStatus(model);
        initMasterMarketingFunnel(model);
        initMasterNationality(model);
        initMasterSourceInfo(model);
        initMasterProvince(model);
        initMasterCity(model);
        initMasterSubDistrict(model);
        initMasterUrbanVillage(model);
        initMasterGroup(model);
        initMasterRole(model);
        initMasterUser(model);
    }

    protected void initAllMasterProfile(Model model){
        initMasterProvince(model);
        initMasterBank(model);
        initMasterEducation(model);
        initMasterGender(model);
        initMasterIncome(model);
        initMasterIncomeSource(model);
        initMasterIndustryCategory(model);
        initMasterInvestmentBudget(model);
        initMasterInvestmentObjective(model);
        initMasterInvestmentRiskPreference(model);
        initMasterInvestmentRolePreference(model);
        initMasterJobPosition(model);
        initMasterJob(model);
        initMasterMaritalStatus(model);
        initMasterNationality(model);
        initMasterSourceInfo(model);
    }

    protected void initAllMasterBusiness(Model model){
        initMasterFormOfBusiness(model);
        initMasterGroup(model);
        initMasterIndustryCategory(model);
        initMasterJobPosition(model);
        initMasterJob(model);
        initMasterManyBranchOfBusiness(model);
        initMasterMarketingFunnel(model);
        initMasterSourceInfo(model);
    }

    protected void initMasterBank(Model model){
        model.addAttribute("bankList",masterRepositoryService.findAll(MasterRepositoryService.REPO_BANK));
    }

    protected void initMasterEducation(Model model){
        model.addAttribute("educationList",masterRepositoryService.findAll(MasterRepositoryService.REPO_EDUCATION));
    }

    protected void initMasterFormOfBusiness(Model model){
        model.addAttribute("formOfBusinessList",masterRepositoryService.findAll(MasterRepositoryService.REPO_FORM_OF_BUSINESS));
    }

    protected void initMasterGender(Model model){
        model.addAttribute("genderList",masterRepositoryService.findAll(MasterRepositoryService.REPO_GENDER));
    }

    protected void initMasterIncome(Model model){
        model.addAttribute("incomeList",masterRepositoryService.findAll(MasterRepositoryService.REPO_INCOME));
    }

    protected void initMasterIncomeSource(Model model){
        model.addAttribute("incomeSourceList",masterRepositoryService.findAll(MasterRepositoryService.REPO_INCOME_SOURCE));
    }

    protected void initMasterIndustryCategory(Model model){
        model.addAttribute("industryCategoryList",masterRepositoryService.findAll(MasterRepositoryService.REPO_INDUSTRY_CATEGORY));
    }

    protected void initMasterInvestmentBudget(Model model){
        model.addAttribute("investmentBudgetList",masterRepositoryService.findAll(MasterRepositoryService.REPO_INVESTMENT_BUDGET));
    }

    protected void initMasterInvestmentObjective(Model model){
        model.addAttribute("investmentObjectiveList",masterRepositoryService.findAll(MasterRepositoryService.REPO_INVESTMENT_OBJECTIVE));
    }

    protected void initMasterInvestmentRiskPreference(Model model){
        model.addAttribute("investmentRiskPreferenceList",masterRepositoryService.findAll(MasterRepositoryService.REPO_INVESTMENT_RISK_PREFERENCE));
    }

    protected void initMasterInvestmentRolePreference(Model model){
        model.addAttribute("investmentRolePreferenceList",masterRepositoryService.findAll(MasterRepositoryService.REPO_INVESTMENT_ROLE_PREFERENCE));
    }

    protected void initMasterJobPosition(Model model){
        model.addAttribute("jobPositionList",masterRepositoryService.findAll(MasterRepositoryService.REPO_JOB_POSITION));
    }

    protected void initMasterJob(Model model){
        model.addAttribute("jobList",masterRepositoryService.findAll(MasterRepositoryService.REPO_JOB));
    }

    protected void initMasterManyBranchOfBusiness(Model model){
        model.addAttribute("manyBranchOfBusinessList",masterRepositoryService.findAll(MasterRepositoryService.REPO_MANY_BRANCH_OF_BUSINESS));
    }

    protected void initMasterMaritalStatus(Model model){
        model.addAttribute("maritalStatusList",masterRepositoryService.findAll(MasterRepositoryService.REPO_MARITAL_STATUS));
    }

    protected void initMasterMarketingFunnel(Model model){
        model.addAttribute("marketingFunnelList",masterRepositoryService.findAll(MasterRepositoryService.REPO_MARKETING_FUNNEL));
    }

    protected void initMasterNationality(Model model){
        model.addAttribute("nationalityList",masterRepositoryService.findAll(MasterRepositoryService.REPO_NATIONALITY));
    }

    protected void initMasterSourceInfo(Model model){
        model.addAttribute("sourceInfoList",masterRepositoryService.findAll(MasterRepositoryService.REPO_SOURCE_INFO));
    }

    protected void initMasterProvince(Model model){
        model.addAttribute("provinceList",masterRepositoryService.findAll(MasterRepositoryService.REPO_PROVINCE));
    }

    protected void initMasterCity(Model model){
        model.addAttribute("cityList",masterRepositoryService.findAll(MasterRepositoryService.REPO_CITY));
    }

    protected void initMasterSubDistrict(Model model){
        model.addAttribute("subDistrictList",masterRepositoryService.findAll(MasterRepositoryService.REPO_SUB_DISTRICT));
    }

    protected void initMasterUrbanVillage(Model model){
        model.addAttribute("urbanVillageList",masterRepositoryService.findAll(MasterRepositoryService.REPO_URBAN_VILLAGE));
    }

    protected void initMasterGroup(Model model){
        model.addAttribute("groupList",masterRepositoryService.findAll(MasterRepositoryService.REPO_GROUP));
    }

    protected void initMasterRole(Model model){
        model.addAttribute("roleList",masterRepositoryService.findAll(MasterRepositoryService.REPO_ROLE));
    }

    protected void initMasterUser(Model model){
        model.addAttribute("userList",masterRepositoryService.findAll(MasterRepositoryService.REPO_USER));
    }

    protected List<City> getCityListByProvince(Province province){
        List<City> cityList = new ArrayList<>();
        if(province == null){
            cityList = cityService.findAllByProvince(province);
        }
        return cityList;
    }

    protected List<SubDistrict> getSubDistrictListByCity(City city){
        List<SubDistrict> subDistrictList = new ArrayList<>();
        if(city == null){
            subDistrictList = subDistrictService.findAllByCity(city);
        }
        return subDistrictList;
    }
}
