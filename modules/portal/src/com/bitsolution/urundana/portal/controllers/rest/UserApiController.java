package com.bitsolution.urundana.portal.controllers.rest;

import com.bitsolution.urundana.entity.master.Bank;
import com.haulmont.cuba.core.app.DataService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.security.entity.User;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

@RestController
public class UserApiController {

    @Inject
    protected DataManager dataManager;

    @RequestMapping(value = "/api/private/user/list", method = RequestMethod.GET)
    public List<User> index(Model model) {
        List<User> list = dataManager.load(User.class).query("select u from sec$User u").view("_local").list();
        System.out.println(list.size());
        return list;
    }
}
