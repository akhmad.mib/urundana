package com.bitsolution.urundana.portal.controllers.base;

import com.bitsolution.urundana.service.SessionService;
import com.haulmont.cuba.core.app.importexport.EntityImportExportService;
import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.portal.security.PortalSessionProvider;
import com.haulmont.cuba.security.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;

@Controller
public class BaseController {

    public EntityImportExportService getEntityImportExportService() {
        return entityImportExportService;
    }

    public void setEntityImportExportService(EntityImportExportService entityImportExportService) {
        this.entityImportExportService = entityImportExportService;
    }

    public SessionService getSessionService() {
        return sessionService;
    }

    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Inject
    private EntityImportExportService entityImportExportService;

    @Inject
    SessionService sessionService;

    public DataManager getDataManager() {
        return dataManager;
    }

    public void setDataManager(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Inject
    DataManager dataManager;

    protected boolean isAnonymous(){
        return !PortalSessionProvider.getUserSession().isAuthenticated();
    }

    protected User getUser(){
        return PortalSessionProvider.getUserSession().getUser();
    }

    protected Collection<String> getRoles(){
        return PortalSessionProvider.getUserSession().getRoles();
    }

    protected String toListJson(Collection<? extends Entity> list){
        return entityImportExportService.exportEntitiesToJSON(list);
    }

    protected String toJson(Entity object){
        Collection<Entity> list = new ArrayList<>();
        list.add(object);
        return entityImportExportService.exportEntitiesToJSON(list);
    }

    protected void initModel(Model model){
        model.addAttribute("userAuth",getUser());
    }
}
