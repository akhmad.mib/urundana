package com.bitsolution.urundana.portal.controllers.rest;

import com.bitsolution.urundana.dto.userprofile.ProfileDto;
import com.bitsolution.urundana.portal.controllers.base.BaseRestController;
import com.bitsolution.urundana.service.SessionService;
import com.bitsolution.urundana.service.userprofile.ProfileService;
import com.haulmont.cuba.core.app.importexport.EntityImportExportService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.portal.sys.security.PortalUserSessionSource;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping("/api/public")
public class ProfileApiController extends BaseRestController {

    @Inject
    protected DataManager dataManager;

    @Inject
    private UserSessionSource userSessionSource;

    @Inject
    private PortalUserSessionSource portalUserSessionSource;

    @Inject
    private EntityImportExportService entityImportExportService;

    @Inject
    ProfileService profileService;

    @Inject
    SessionService sessionService;

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String profile(Model model) {
        System.out.println(getUser());
        System.out.println(isAnonymous());
        return toListJson(profileService.getAll());
    }

    @RequestMapping(value = "/profile2", method = RequestMethod.GET)
    public List<ProfileDto> profile2(Model model) {
        System.out.println(getUser());
        System.out.println(isAnonymous());
        return profileService.getAllDto();
    }
}
