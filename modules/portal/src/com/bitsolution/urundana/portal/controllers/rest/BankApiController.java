package com.bitsolution.urundana.portal.controllers.rest;

import com.bitsolution.urundana.entity.master.Bank;
import com.bitsolution.urundana.portal.controllers.base.BaseRestController;
import com.bitsolution.urundana.service.master.BankService;
import com.bitsolution.urundana.service.SessionService;
import com.bitsolution.urundana.service.userprofile.BankProfileService;
import com.haulmont.cuba.core.app.importexport.EntityImportExportService;
import com.haulmont.cuba.core.app.serialization.EntitySerializationAPI;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.portal.sys.security.PortalUserSessionSource;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/public")
public class BankApiController extends BaseRestController {

    @Inject
    protected DataManager dataManager;

    @Inject
    private UserSessionSource userSessionSource;

    @Inject
    private PortalUserSessionSource portalUserSessionSource;

    @Inject
    private EntityImportExportService entityImportExportService;


    @Inject
    BankProfileService bankProfileService;


    @Inject
    BankService bankService;

    @Inject
    SessionService sessionService;

    @RequestMapping(value = "/bank", method = RequestMethod.GET)
    public String bank(Model model) {
        System.out.println(getUser());
        System.out.println(isAnonymous());
        return toListJson(bankService.getAll());
    }

    @RequestMapping(value = "/bank2", method = RequestMethod.GET)
    public String bank2(Model model) {
        System.out.println(getUser());
        System.out.println(isAnonymous());
        return toListJson(bankService.findByOrderByLabelAsc());
    }

    @RequestMapping(value = "/bank3", method = RequestMethod.GET)
    public String bank3(Model model) {
        System.out.println(getUser());
        System.out.println(isAnonymous());
        return toJson(bankService.findFirstByOrderByLabelDesc());
    }

    @RequestMapping(value = "/bank4", method = RequestMethod.GET)
    public String bank4(Model model) {
        System.out.println(getUser());
        System.out.println(isAnonymous());
        return String.format("%s",bankService.countAll());
    }

    @RequestMapping(value = "/bank5", method = RequestMethod.GET)
    public String bank5(Model model) {
        System.out.println(getUser());
        System.out.println(isAnonymous());
        return toListJson(bankService.findPageByOrderByLabelAsc().getContent());
    }

    @RequestMapping(value = "/bank6", method = RequestMethod.GET)
    public String bank6(Model model) {
        System.out.println(getUser());
        System.out.println(isAnonymous());
        return toJson(bankProfileService.findByUser(getUser()));
    }

    @RequestMapping(value = "/bank7", method = RequestMethod.GET)
    public String bank7(Model model) {
        System.out.println(getUser());
        System.out.println(isAnonymous());
        return toJson(bankProfileService.findByUserId(getUser().getId()));
    }
}
