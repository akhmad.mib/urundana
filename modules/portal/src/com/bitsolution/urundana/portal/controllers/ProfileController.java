package com.bitsolution.urundana.portal.controllers;

import com.bitsolution.urundana.dto.userprofile.PersonalDataProfileDto;
import com.bitsolution.urundana.dto.userprofile.RegisterUserDto;
import com.bitsolution.urundana.dto.userprofile.UserProfileDto;
import com.bitsolution.urundana.entity.master.Bank;
import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.userprofile.*;
import com.bitsolution.urundana.mapper.userprofile.UserProfileMapper;
import com.bitsolution.urundana.portal.controllers.base.BaseController;
import com.bitsolution.urundana.portal.controllers.base.BaseMasterController;
import com.bitsolution.urundana.result.base.ActionResult;
import com.bitsolution.urundana.service.master.BankService;
import com.bitsolution.urundana.service.master.GenderService;
import com.bitsolution.urundana.service.userprofile.PersonalDataProfileService;
import com.bitsolution.urundana.service.userprofile.UserProfileService;
import com.bitsolution.urundana.util.CustomBeanUtils;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;
import com.haulmont.cuba.core.global.Metadata;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.List;

@Controller
public class ProfileController extends BaseMasterController {

    @Inject
    UserProfileService userProfileService;

    @Inject
    PersonalDataProfileService personalDataProfileService;

    @GetMapping(value = "profile")
    public String profile(Model model) {
        initModel(model);
        initAllMasterProfile(model);
        UserProfile userProfile = userProfileService.findByUserId(getUser().getId(),"userprofile-full-view");
        model.addAttribute("personalDataProfile",userProfile.getPersonalData());
        model.addAttribute("jobProfile",userProfile.getJob());
        model.addAttribute("bankProfile",userProfile.getBank());
        model.addAttribute("documentProfile",userProfile.getDocument());
        model.addAttribute("preferenceProfile",userProfile.getPreference());
        return "cuba/profile";
    }

    @PostMapping(value = "profile")
    @ResponseBody
    public ActionResult userProfileUpdate(UserProfileDto userProfileDto, Model model) {
        ActionResult result = new ActionResult();
        try {
            UserProfile userProfile = userProfileService.findByUserId(getUser().getId(), "userprofile-full-view");
            CustomBeanUtils.copyProperties(userProfileDto, userProfile, "id", "user",
                    "personalData.user", "personalData.id",
                    "job.user", "job.id",
                    "document.user", "document.id",
                    "bank.user", "bank",
                    "preference.user", "preference.id");
            userProfileService.persist(userProfile);
            result.setSuccess(true);
        }catch (Exception ex){
            result.setSuccess(false);
            result.setMessage(ex.getMessage());
        }
        return result;
    }

    @PostMapping(value = "personalDataProfile")
    @ResponseBody
    public ActionResult personalDataProfileUpdate(PersonalDataProfileDto personalDataProfileDto, Model model) {
        ActionResult result = new ActionResult();
        try {
            PersonalDataProfile personalDataProfile = personalDataProfileService.findByUserId(getUser().getId(), "personaldataprofile-full-view");
            CustomBeanUtils.copyProperties(personalDataProfileDto, personalDataProfile, "id", "user");
            personalDataProfileService.persist(personalDataProfile);
            result.setSuccess(true);
        }catch (Exception ex){
            result.setSuccess(false);
            result.setMessage(ex.getMessage());
        }
        return result;
    }
}
