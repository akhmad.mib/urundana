package com.bitsolution.urundana.service;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.urundana.repository.master.*;
import com.haulmont.cuba.core.entity.Entity;

import java.io.Serializable;
import java.util.Optional;

public interface MasterRepositoryService {
    String NAME = "urundana_MasterRepositoryService";

    String REPO_BANK = "BANK";
    String REPO_CITY = "CITY";
    String REPO_EDUCATION = "EDUCATION";
    String REPO_FORM_OF_BUSINESS = "FORM_OF_BUSINESS";
    String REPO_GENDER = "GENDER";
    String REPO_INCOME = "INCOME";
    String REPO_INCOME_SOURCE = "INCOME_SOURCE";
    String REPO_INDUSTRY_CATEGORY = "INDUSTRY_CATEGORY";
    String REPO_INVESTMENT_BUDGET = "INVESTMENT_BUDGET";
    String REPO_INVESTMENT_OBJECTIVE = "INVESTMENT_OBJECTIVE";
    String REPO_INVESTMENT_RISK_PREFERENCE = "INVESTMENT_RISK_PREFERENCE";
    String REPO_INVESTMENT_ROLE_PREFERENCE = "INVESTMENT_ROLE_PREFERENCE";
    String REPO_JOB_POSITION = "JOB_POSITION";
    String REPO_JOB = "JOB";
    String REPO_MANY_BRANCH_OF_BUSINESS = "MANY_BRANCH_OF_BUSINESS";
    String REPO_MARITAL_STATUS = "MARITAL_STATUS";
    String REPO_MARKETING_FUNNEL = "MARKETING_FUNNEL";
    String REPO_NATIONALITY = "NATIONALITY";
    String REPO_PROVINCE = "PROVINCE";
    String REPO_SOURCE_INFO = "SOURCE_INFO";
    String REPO_SUB_DISTRICT = "SUB_DISTRICT";
    String REPO_URBAN_VILLAGE = "URBAN_VILLAGE";
    String REPO_ROLE = "ROLE";
    String REPO_GROUP = "GROUP";
    String REPO_USER = "USER";

//    BankRepository getBankRepository();
//
//    CityRepository getCityRepository();
//
//    EducationRepository getEducationRepository();
//
//    FormOfBusinessRepository getFormOfBusinessRepository();
//
//    GenderRepository getGenderRepository();
//
//    GroupRepository getGroupRepository();
//
//    IncomeRepository getIncomeRepository();
//
//    IncomeSourceRepository getIncomeSourceRepository();
//
//    IndustryCategoryRepository getIndustryCategoryRepository();
//
//    InvestmentBudgetRepository getInvestmentBudgetRepository();
//
//    InvestmentObjectiveRepository getInvestmentObjectiveRepository();
//
//    InvestmentRiskPreferenceRepository getInvestmentRiskPreferenceRepository();
//
//    InvestmentRolePreferenceRepository getInvestmentRolePreferenceRepository();
//
//    JobPositionRepository getJobPositionRepository();
//
//    JobRepository getJobRepository();
//
//    ManyBranchOfBusinessRepository getManyBranchOfBusinessRepository();
//
//    MaritalStatusRepository getMaritalStatusRepository();
//
//    MarketingFunnelRepository getMarketingFunnelRepository();
//
//    NationalityRepository getNationalityRepository();
//
//    ProvinceRepository getProvinceRepository();
//
//    RoleRepository getRoleRepository();
//
//    SourceInfoRepository getSourceInfoRepository();
//
//    SubDistrictRepository getSubDistrictRepository();
//
//    UrbanVillageRepository getUrbanVillageRepository();
//
//    UserRepository getUserRepository();

    public Entity findOne(String repositoryName, Serializable serializable, String view);

    public Iterable findAll(String repositoryName, String view);

    public Iterable findAll(String repositoryName, Iterable iterable, String view);

    public String getDefaultViewName(String repositoryName);

    public void setDefaultViewName(String repositoryName, String defaultViewName);

    public Object save(String repositoryName, Object entity);

    public Iterable saveAll(String repositoryName, Iterable entities);

    public Optional findById(String repositoryName, Object o);

    public boolean existsById(String repositoryName, Object o);

    public Iterable findAll(String repositoryName);

    public Iterable findAllById(String repositoryName, Iterable iterable);

    public long count(String repositoryName);

    public void deleteById(String repositoryName, Object o);

    public void delete(String repositoryName, Object entity);

    public void deleteAll(String repositoryName, Iterable entities);

    public void deleteAll(String repositoryName);

}