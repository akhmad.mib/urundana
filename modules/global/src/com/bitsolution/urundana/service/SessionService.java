package com.bitsolution.urundana.service;

import com.haulmont.cuba.security.global.UserSession;

public interface SessionService {
    String NAME = "urundana_SessionService";

    public boolean isAnonymous(UserSession userSession);
}