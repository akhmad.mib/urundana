package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.InvestmentBudget;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.InvestmentBudgetRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;

public interface InvestmentBudgetService extends BaseCrudService<InvestmentBudget,String, InvestmentBudgetRepository> {
    String NAME = "urundana_InvestmentBudgetService";
}