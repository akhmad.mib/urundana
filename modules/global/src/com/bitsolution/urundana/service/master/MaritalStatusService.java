package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.MaritalStatus;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.MaritalStatusRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;

public interface MaritalStatusService extends BaseCrudService<MaritalStatus,String, MaritalStatusRepository> {
    String NAME = "urundana_MaritalStatusService";
}