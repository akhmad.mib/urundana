package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Nationality;
import com.bitsolution.urundana.entity.master.Province;
import com.bitsolution.urundana.repository.master.NationalityRepository;
import com.bitsolution.urundana.repository.master.ProvinceRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;

public interface ProvinceService extends BaseCrudService<Province,Integer, ProvinceRepository> {
    String NAME = "urundana_ProvinceService";
}