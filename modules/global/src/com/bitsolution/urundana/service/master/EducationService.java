package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Education;
import com.bitsolution.urundana.entity.master.MaritalStatus;
import com.bitsolution.urundana.repository.master.EducationRepository;
import com.bitsolution.urundana.repository.master.MaritalStatusRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;

public interface EducationService extends BaseCrudService<Education,String, EducationRepository> {
    String NAME = "urundana_EducationService";
}