package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.InvestmentRiskPreference;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.InvestmentRiskPreferenceRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;

public interface InvestmentRiskPreferenceService extends BaseCrudService<InvestmentRiskPreference,String, InvestmentRiskPreferenceRepository> {
    String NAME = "urundana_InvestmentRiskPreferenceService";
}