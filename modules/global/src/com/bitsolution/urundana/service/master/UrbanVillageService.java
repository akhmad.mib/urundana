package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Province;
import com.bitsolution.urundana.entity.master.UrbanVillage;
import com.bitsolution.urundana.repository.master.ProvinceRepository;
import com.bitsolution.urundana.repository.master.UrbanVillageRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;

public interface UrbanVillageService extends BaseCrudService<UrbanVillage,Integer, UrbanVillageRepository> {
    String NAME = "urundana_UrbanVillageService";
}