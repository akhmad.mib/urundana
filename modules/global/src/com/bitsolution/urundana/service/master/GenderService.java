package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Bank;
import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.repository.master.BankRepository;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;

public interface GenderService extends BaseCrudService<Gender,String, GenderRepository> {
    String NAME = "urundana_GenderService";
}