package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.InvestmentObjective;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.InvestmentObjectiveRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;

public interface InvestmentObjectiveService extends BaseCrudService<InvestmentObjective,String, InvestmentObjectiveRepository> {
    String NAME = "urundana_InvestmentObjectiveService";
}