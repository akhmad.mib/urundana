package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.IndustryCategory;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.IndustryCategoryRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;

public interface IndustryCategoryService extends BaseCrudService<IndustryCategory,String, IndustryCategoryRepository> {
    String NAME = "urundana_IndustryCategoryService";
}