package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.InvestmentRolePreference;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.InvestmentRolePreferenceRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;

public interface InvestmentRolePreferenceService extends BaseCrudService<InvestmentRolePreference,String, InvestmentRolePreferenceRepository> {
    String NAME = "urundana_InvestmentRolePreferenceService";
}