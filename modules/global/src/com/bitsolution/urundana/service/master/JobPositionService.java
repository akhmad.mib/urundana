package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.JobPosition;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.JobPositionRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;

public interface JobPositionService extends BaseCrudService<JobPosition,String, JobPositionRepository> {
    String NAME = "urundana_JobPositionService";
}