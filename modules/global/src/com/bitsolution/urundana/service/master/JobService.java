package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.Job;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.JobRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;

public interface JobService extends BaseCrudService<Job,String, JobRepository> {
    String NAME = "urundana_JobService";
}