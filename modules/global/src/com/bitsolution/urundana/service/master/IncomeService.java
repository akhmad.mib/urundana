package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.Income;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.IncomeRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;

public interface IncomeService extends BaseCrudService<Income,String, IncomeRepository> {
    String NAME = "urundana_IncomeService";
}