package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.City;
import com.bitsolution.urundana.entity.master.Province;
import com.bitsolution.urundana.entity.master.SubDistrict;
import com.bitsolution.urundana.repository.master.ProvinceRepository;
import com.bitsolution.urundana.repository.master.SubDistrictRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SubDistrictService extends BaseCrudService<SubDistrict,Integer, SubDistrictRepository> {
    String NAME = "urundana_SubDistrictService";
    List<SubDistrict> findAllByCity(City city);
}