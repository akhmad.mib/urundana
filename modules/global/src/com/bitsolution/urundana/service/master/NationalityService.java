package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.Nationality;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.NationalityRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;

public interface NationalityService extends BaseCrudService<Nationality,String, NationalityRepository> {
    String NAME = "urundana_NationalityService";
}