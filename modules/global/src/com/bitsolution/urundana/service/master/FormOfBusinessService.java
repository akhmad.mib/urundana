package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Education;
import com.bitsolution.urundana.entity.master.FormOfBusiness;
import com.bitsolution.urundana.repository.master.EducationRepository;
import com.bitsolution.urundana.repository.master.FormOfBusinessRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;

public interface FormOfBusinessService extends BaseCrudService<FormOfBusiness,String, FormOfBusinessRepository> {
    String NAME = "urundana_FormOfBusinessService";
}