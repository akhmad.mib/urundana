package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.ManyBranchOfBusiness;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.ManyBranchOfBusinessRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;

public interface ManyBranchOfBusinessService extends BaseCrudService<ManyBranchOfBusiness,String, ManyBranchOfBusinessRepository> {
    String NAME = "urundana_ManyBranchOfBusinessService";
}