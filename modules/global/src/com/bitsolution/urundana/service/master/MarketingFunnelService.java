package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.MarketingFunnel;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.MarketingFunnelRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;

public interface MarketingFunnelService extends BaseCrudService<MarketingFunnel,String, MarketingFunnelRepository> {
    String NAME = "urundana_MarketingFunnelService";
}