package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.City;
import com.bitsolution.urundana.entity.master.Province;
import com.bitsolution.urundana.repository.master.CityRepository;
import com.bitsolution.urundana.repository.master.ProvinceRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CityService extends BaseCrudService<City,Integer, CityRepository> {
    String NAME = "urundana_CityService";
    List<City> findAllByProvince(Province province);
}