package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.SourceInfo;
import com.bitsolution.urundana.repository.master.GenderRepository;
import com.bitsolution.urundana.repository.master.SourceInfoRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;

public interface SourceInfoService extends BaseCrudService<SourceInfo,String, SourceInfoRepository> {
    String NAME = "urundana_SourceInfoService";
}