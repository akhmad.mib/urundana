package com.bitsolution.urundana.service.master;

import com.bitsolution.urundana.entity.master.Bank;
import com.bitsolution.urundana.repository.master.BankRepository;
import com.bitsolution.urundana.service.base.BaseCrudService;
import org.springframework.data.domain.Page;

import java.util.List;

public interface BankService extends BaseCrudService<Bank,String, BankRepository> {
    String NAME = "urundana_BankService";

    public List<Bank> getAll();

    public Long countAll();

    public Bank findFirstByOrderByLabelDesc();

    public List<Bank> findByOrderByLabelAsc();

    public Page<Bank> findPageByOrderByLabelAsc();
}