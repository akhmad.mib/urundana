package com.bitsolution.urundana.service.userprofile;

import com.bitsolution.urundana.entity.userprofile.BankProfile;
import com.bitsolution.urundana.entity.userprofile.DocumentProfile;
import com.bitsolution.urundana.repository.userprofile.BankProfileRepository;
import com.bitsolution.urundana.repository.userprofile.DocumentProfileRepository;
import com.bitsolution.urundana.service.base.BaseByUserService;

public interface DocumentProfileService extends BaseByUserService<DocumentProfile,String, DocumentProfileRepository> {
    String NAME = "urundana_DocumentProfileService";
}