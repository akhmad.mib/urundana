package com.bitsolution.urundana.service.userprofile;

import com.bitsolution.urundana.entity.userprofile.BankProfile;
import com.bitsolution.urundana.entity.userprofile.UserProfile;
import com.bitsolution.urundana.repository.userprofile.UserProfileRepository;
import com.bitsolution.urundana.service.base.BaseByUserService;

public interface UserProfileService extends BaseByUserService<UserProfile,String, UserProfileRepository> {
    String NAME = "urundana_UserProfileService";
}