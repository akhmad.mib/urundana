package com.bitsolution.urundana.service.userprofile;

import com.bitsolution.urundana.dto.userprofile.ProfileDto;
import com.bitsolution.urundana.entity.userprofile.Profile;

import java.util.List;

public interface ProfileService {
    String NAME = "urundana_ProfileService";

    public List<Profile> getAll();

    public List<ProfileDto> getAllDto();
}