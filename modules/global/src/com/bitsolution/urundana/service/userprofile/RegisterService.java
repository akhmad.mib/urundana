package com.bitsolution.urundana.service.userprofile;

import com.bitsolution.urundana.dto.userprofile.RegisterUserDto;
import com.bitsolution.urundana.result.userprofile.RegisterUserActionResult;

public interface RegisterService {
    String NAME = "urundana_RegisterService";
    RegisterUserActionResult registerUser(RegisterUserDto registerUserDto);
}