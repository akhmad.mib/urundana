package com.bitsolution.urundana.service.userprofile;

import com.bitsolution.urundana.entity.userprofile.BankProfile;
import com.bitsolution.urundana.repository.userprofile.BankProfileRepository;
import com.bitsolution.urundana.service.base.BaseByUserService;

public interface BankProfileService extends BaseByUserService<BankProfile,String, BankProfileRepository> {
    String NAME = "urundana_BankProfileService";
}