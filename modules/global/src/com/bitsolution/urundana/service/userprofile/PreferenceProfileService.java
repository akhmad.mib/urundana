package com.bitsolution.urundana.service.userprofile;

import com.bitsolution.urundana.entity.userprofile.BankProfile;
import com.bitsolution.urundana.entity.userprofile.PreferenceProfile;
import com.bitsolution.urundana.repository.userprofile.BankProfileRepository;
import com.bitsolution.urundana.repository.userprofile.PreferenceProfileRepository;
import com.bitsolution.urundana.service.base.BaseByUserService;

public interface PreferenceProfileService extends BaseByUserService<PreferenceProfile,String, PreferenceProfileRepository> {
    String NAME = "urundana_PreferenceProfileService";
}