package com.bitsolution.urundana.service.userprofile;

import com.bitsolution.urundana.entity.userprofile.BankProfile;
import com.bitsolution.urundana.entity.userprofile.PersonalDataProfile;
import com.bitsolution.urundana.repository.userprofile.BankProfileRepository;
import com.bitsolution.urundana.repository.userprofile.PersonalDataProfileRepository;
import com.bitsolution.urundana.service.base.BaseByUserService;

public interface PersonalDataProfileService extends BaseByUserService<PersonalDataProfile,String, PersonalDataProfileRepository> {
    String NAME = "urundana_PersonalDataService";
}