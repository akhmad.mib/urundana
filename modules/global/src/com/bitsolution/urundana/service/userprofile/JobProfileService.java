package com.bitsolution.urundana.service.userprofile;

import com.bitsolution.urundana.entity.userprofile.BankProfile;
import com.bitsolution.urundana.entity.userprofile.JobProfile;
import com.bitsolution.urundana.repository.userprofile.BankProfileRepository;
import com.bitsolution.urundana.repository.userprofile.JobProfileRepository;
import com.bitsolution.urundana.service.base.BaseByUserService;

public interface JobProfileService extends BaseByUserService<JobProfile,String, JobProfileRepository> {
    String NAME = "urundana_JobProfileService";
}