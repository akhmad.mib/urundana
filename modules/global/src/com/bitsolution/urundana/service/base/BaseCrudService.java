package com.bitsolution.urundana.service.base;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.haulmont.cuba.core.entity.Entity;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

public interface BaseCrudService <T extends Entity,ID extends Serializable, R extends SimpleCubaJpaRepository> extends SimpleCubaJpaRepository {
    String NAME = "urundana_BaseCrudService";

    @Transactional
    void persist(T entity);
}