package com.bitsolution.urundana.service.base;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.security.entity.User;

import java.io.Serializable;
import java.util.UUID;

public interface BaseByUserService<T extends Entity,ID extends Serializable, R extends SimpleCubaJpaRepository> extends BaseCrudService<T,ID,R> {
    String NAME = "urundana_BaseByUserService";

    T findByUser(User user);

    T findByUserId(UUID userId);

    T findByUser(User user, String view);

    T findByUserId(UUID userId, String view);
}