package com.bitsolution.urundana.dto.userprofile;

import com.bitsolution.urundana.entity.master.*;
import com.bitsolution.urundana.entity.userprofile.Profile;
import com.bitsolution.urundana.entity.userprofile.UserInvestmentObjective;
import com.bitsolution.urundana.entity.userprofile.UserInvestmentPreference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haulmont.cuba.core.entity.FileDescriptor;
import com.haulmont.cuba.security.entity.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ProfileDto implements Serializable {

    private String firstName;

    private String lastName;

    @JsonIgnore
    private Gender gender;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getBirthOfPlace() {
        return birthOfPlace;
    }

    public void setBirthOfPlace(String birthOfPlace) {
        this.birthOfPlace = birthOfPlace;
    }

    public Date getBirthOfDate() {
        return birthOfDate;
    }

    public void setBirthOfDate(Date birthOfDate) {
        this.birthOfDate = birthOfDate;
    }

    public Nationality getNationality() {
        return nationality;
    }

    public void setNationality(Nationality nationality) {
        this.nationality = nationality;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getProfilBackground() {
        return profilBackground;
    }

    public void setProfilBackground(String profilBackground) {
        this.profilBackground = profilBackground;
    }

    private String birthOfPlace;

    private Date birthOfDate;
    @JsonIgnore
    private Nationality nationality;
    @JsonIgnore
    private MaritalStatus maritalStatus;

    private String profilBackground;

}
