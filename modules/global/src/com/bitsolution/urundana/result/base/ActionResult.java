package com.bitsolution.urundana.result.base;

public class ActionResult<T> extends BaseActionResult{
    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    private T data;
}
