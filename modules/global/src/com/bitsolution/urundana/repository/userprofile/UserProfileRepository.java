package com.bitsolution.urundana.repository.userprofile;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.urundana.entity.userprofile.UserProfile;

import java.util.UUID;

public interface UserProfileRepository  extends SimpleCubaJpaRepository<UserProfile, String> {
}
