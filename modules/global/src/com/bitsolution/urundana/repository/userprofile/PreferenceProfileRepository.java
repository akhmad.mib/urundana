package com.bitsolution.urundana.repository.userprofile;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.urundana.entity.userprofile.BankProfile;
import com.bitsolution.urundana.entity.userprofile.PreferenceProfile;

import java.util.UUID;

public interface PreferenceProfileRepository  extends SimpleCubaJpaRepository<PreferenceProfile, String> {
}
