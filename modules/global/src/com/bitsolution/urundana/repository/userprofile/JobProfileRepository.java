package com.bitsolution.urundana.repository.userprofile;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.urundana.entity.userprofile.BankProfile;
import com.bitsolution.urundana.entity.userprofile.JobProfile;

import java.util.UUID;

public interface JobProfileRepository extends SimpleCubaJpaRepository<JobProfile, String> {
}
