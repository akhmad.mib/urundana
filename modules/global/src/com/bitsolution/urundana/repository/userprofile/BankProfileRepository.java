package com.bitsolution.urundana.repository.userprofile;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.urundana.entity.userprofile.BankProfile;
import com.haulmont.cuba.security.entity.User;

import java.util.UUID;

public interface BankProfileRepository extends SimpleCubaJpaRepository<BankProfile, String> {
}
