package com.bitsolution.urundana.repository.master;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.urundana.entity.master.Job;
import com.bitsolution.urundana.entity.master.MarketingFunnel;

public interface MarketingFunnelRepository extends SimpleCubaJpaRepository<MarketingFunnel, String> {

}
