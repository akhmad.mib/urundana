package com.bitsolution.urundana.repository.master;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.urundana.entity.master.InvestmentRiskPreference;
import com.bitsolution.urundana.entity.master.Job;

public interface InvestmentRiskPreferenceRepository extends SimpleCubaJpaRepository<InvestmentRiskPreference, String> {

}
