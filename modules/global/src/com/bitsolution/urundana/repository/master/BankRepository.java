package com.bitsolution.urundana.repository.master;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaView;
import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleJpqlQuery;
import com.bitsolution.urundana.entity.master.Bank;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.UUID;

public interface BankRepository extends SimpleCubaJpaRepository<Bank, String> {

    @SimpleJpqlQuery("SELECT Count(Bank) FROM urundana_Bank Bank")
    Long countAll();

    @SimpleCubaView("_local")
    @SimpleJpqlQuery("SELECT  Bank FROM urundana_Bank Bank ORDER BY Bank.label ASC")
    Bank findFirstByOrderByLabelDesc();

    @SimpleCubaView("_local")
    @SimpleJpqlQuery("SELECT  Bank FROM urundana_Bank Bank ORDER BY Bank.label ASC")
    List<Bank> findAllByOrderByLabelAsc(Pageable pageable);

    @SimpleCubaView("_local")
    @SimpleJpqlQuery(value = "SELECT  Bank FROM urundana_Bank Bank ORDER BY Bank.label ASC",
                     countQuery = "SELECT Count(Bank) FROM urundana_Bank Bank")
    Page<Bank> findPageByOrderByLabelAsc(Pageable pageable);

}
