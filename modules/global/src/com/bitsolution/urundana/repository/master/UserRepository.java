package com.bitsolution.urundana.repository.master;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.urundana.entity.master.Bank;
import com.haulmont.cuba.security.entity.User;

import java.util.UUID;

public interface UserRepository extends SimpleCubaJpaRepository<User, UUID> {
}
