package com.bitsolution.urundana.repository.master;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.urundana.entity.master.Gender;
import com.bitsolution.urundana.entity.master.MaritalStatus;

public interface MaritalStatusRepository extends SimpleCubaJpaRepository<MaritalStatus, String> {

}
