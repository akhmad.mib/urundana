package com.bitsolution.urundana.repository.master;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.urundana.entity.master.InvestmentBudget;
import com.bitsolution.urundana.entity.master.Job;

public interface InvestmentBudgetRepository extends SimpleCubaJpaRepository<InvestmentBudget, String> {

}
