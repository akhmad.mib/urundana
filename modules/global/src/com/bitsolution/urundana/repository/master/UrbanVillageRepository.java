package com.bitsolution.urundana.repository.master;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.urundana.entity.master.Province;
import com.bitsolution.urundana.entity.master.UrbanVillage;

public interface UrbanVillageRepository extends SimpleCubaJpaRepository<UrbanVillage, Integer> {

}
