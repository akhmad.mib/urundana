package com.bitsolution.urundana.repository.master;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.haulmont.cuba.security.entity.Role;
import com.haulmont.cuba.security.entity.User;

import java.util.UUID;

public interface RoleRepository extends SimpleCubaJpaRepository<Role, UUID> {
}
