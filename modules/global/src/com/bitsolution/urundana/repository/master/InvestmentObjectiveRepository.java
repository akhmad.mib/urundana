package com.bitsolution.urundana.repository.master;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.urundana.entity.master.InvestmentObjective;
import com.bitsolution.urundana.entity.master.Job;

public interface InvestmentObjectiveRepository extends SimpleCubaJpaRepository<InvestmentObjective, String> {

}
