package com.bitsolution.urundana.repository.master;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.urundana.entity.master.Job;
import com.bitsolution.urundana.entity.master.SourceInfo;

public interface SourceInfoRepository extends SimpleCubaJpaRepository<SourceInfo, String> {

}
