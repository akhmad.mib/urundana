package com.bitsolution.urundana.repository.master;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaView;
import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleJpqlQuery;
import com.bitsolution.urundana.entity.master.Bank;
import com.bitsolution.urundana.entity.master.City;
import com.bitsolution.urundana.entity.master.Province;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CityRepository extends SimpleCubaJpaRepository<City, Integer> {
    @SimpleCubaView("_local")
    @SimpleJpqlQuery("SELECT City FROM urundana_City City WHERE City.province = :province")
    List<City> findAllByProvince(@Param("province") Province province);
}