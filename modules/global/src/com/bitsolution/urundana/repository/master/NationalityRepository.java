package com.bitsolution.urundana.repository.master;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaView;
import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleJpqlQuery;
import com.bitsolution.urundana.entity.master.Bank;
import com.bitsolution.urundana.entity.master.Nationality;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface NationalityRepository extends SimpleCubaJpaRepository<Nationality, String> {

}
