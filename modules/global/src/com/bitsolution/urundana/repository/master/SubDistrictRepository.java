package com.bitsolution.urundana.repository.master;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaView;
import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleJpqlQuery;
import com.bitsolution.urundana.entity.master.City;
import com.bitsolution.urundana.entity.master.Province;
import com.bitsolution.urundana.entity.master.SubDistrict;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SubDistrictRepository extends SimpleCubaJpaRepository<SubDistrict, Integer> {
    @SimpleCubaView("_local")
    @SimpleJpqlQuery("SELECT SubDistrict FROM urundana_SubDistrict SubDistrict WHERE SubDistrict.city = :city")
    List<SubDistrict> findAllByCity(@Param("city") City city);
}
