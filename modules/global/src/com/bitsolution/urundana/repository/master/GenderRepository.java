package com.bitsolution.urundana.repository.master;

import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaJpaRepository;
import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleCubaView;
import com.bitsolution.addons.simple.cuba.jpa.repositories.config.SimpleJpqlQuery;
import com.bitsolution.urundana.entity.master.Bank;
import com.bitsolution.urundana.entity.master.Gender;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.UUID;

public interface GenderRepository extends SimpleCubaJpaRepository<Gender, String>{

}
