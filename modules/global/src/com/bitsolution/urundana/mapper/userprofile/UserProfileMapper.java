package com.bitsolution.urundana.mapper.userprofile;

import com.bitsolution.urundana.dto.userprofile.ProfileDto;
import com.bitsolution.urundana.dto.userprofile.UserProfileDto;
import com.bitsolution.urundana.entity.userprofile.Profile;
import com.bitsolution.urundana.entity.userprofile.UserProfile;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface UserProfileMapper {
    UserProfileMapper INSTANCE = Mappers.getMapper(UserProfileMapper.class);

    @Mapping(target = "id", ignore = true)
    UserProfileDto userProfileToUserProfileDto(UserProfile profile);

    @Mapping(target = "id", ignore = true)
    UserProfile userProfileDtoToUserProfile(UserProfileDto userProfileDto);

    @Mapping(target = "id", ignore = true)
    List<ProfileDto> userProfilesToUserProfilesDto(List<UserProfile> profile);
}
