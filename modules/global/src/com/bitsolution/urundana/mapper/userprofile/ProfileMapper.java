package com.bitsolution.urundana.mapper.userprofile;

import com.bitsolution.urundana.dto.userprofile.ProfileDto;
import com.bitsolution.urundana.entity.userprofile.Profile;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ProfileMapper {
    ProfileMapper INSTANCE = Mappers.getMapper(ProfileMapper.class);

    ProfileDto profileToProfileDto(Profile profile);

    @Mapping(target = "id", ignore = true)
    List<ProfileDto> profilesToProfilesDto(List<Profile> profile);
}
