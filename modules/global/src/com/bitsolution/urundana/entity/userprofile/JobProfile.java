package com.bitsolution.urundana.entity.userprofile;

import com.bitsolution.urundana.entity.base.SnowflakeStandardEntity;
import com.bitsolution.urundana.entity.master.*;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;
import com.haulmont.cuba.security.entity.User;

import javax.persistence.*;

@Table(name = "URUNDANA_JOB_PROFILE")
@Entity(name = "urundana_JobProfile")
@NamePattern("%s|job")
public class JobProfile extends SnowflakeStandardEntity {
    private static final long serialVersionUID = -7859751687325878290L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    @OnDeleteInverse(DeletePolicy.CASCADE)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LAST_EDUCATION_ID")
    private Education lastEducation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "JOB_ID")
    private Job job;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INDUSTRY_CATEGORY_ID")
    private IndustryCategory industryCategory;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INCOME_ID")
    private Income income;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INCOME_SOURCE_ID")
    private IncomeSource incomeSource;

    @Lob
    @Column(name = "BUSINESS_DESCRIPTION")
    private String businessDescription;

    @Lob
    @Column(name = "INCOME_SOURCE_DESCRIPTION")
    private String incomeSourceDescription;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getIncomeSourceDescription() {
        return incomeSourceDescription;
    }

    public void setIncomeSourceDescription(String incomeSourceDescription) {
        this.incomeSourceDescription = incomeSourceDescription;
    }

    public String getBusinessDescription() {
        return businessDescription;
    }

    public void setBusinessDescription(String businessDescription) {
        this.businessDescription = businessDescription;
    }

    public IncomeSource getIncomeSource() {
        return incomeSource;
    }

    public void setIncomeSource(IncomeSource incomeSource) {
        this.incomeSource = incomeSource;
    }

    public Income getIncome() {
        return income;
    }

    public void setIncome(Income income) {
        this.income = income;
    }

    public IndustryCategory getIndustryCategory() {
        return industryCategory;
    }

    public void setIndustryCategory(IndustryCategory industryCategory) {
        this.industryCategory = industryCategory;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Education getLastEducation() {
        return lastEducation;
    }

    public void setLastEducation(Education lastEducation) {
        this.lastEducation = lastEducation;
    }
}