package com.bitsolution.urundana.entity.userprofile;

import com.bitsolution.urundana.entity.base.SnowflakeStandardEntity;
import com.bitsolution.urundana.entity.master.InvestmentBudget;
import com.bitsolution.urundana.entity.master.InvestmentRiskPreference;
import com.bitsolution.urundana.entity.master.InvestmentRolePreference;
import com.bitsolution.urundana.entity.master.SourceInfo;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;
import com.haulmont.cuba.security.entity.User;

import javax.persistence.*;
import java.util.List;

@Table(name = "URUNDANA_PREFERENCE_PROFILE")
@Entity(name = "urundana_PreferenceProfile")
public class PreferenceProfile extends SnowflakeStandardEntity {
    private static final long serialVersionUID = -6543978699395713521L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    @OnDeleteInverse(DeletePolicy.CASCADE)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INVESTMENT_BUDGET_ID")
    private InvestmentBudget investmentBudget;

    @OneToMany(mappedBy = "preferenceProfile")
    private List<UserInvestmentObjective> investmentObjectiveList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INVESTMENT_RISK_PREFERENCE_ID")
    private InvestmentRiskPreference investmentRiskPreference;

    @OneToMany(mappedBy = "preferenceProfile")
    private List<UserInvestmentPreference> investmentPreferenceList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INVESTMENT_ROLE_PREFERENCE_ID")
    private InvestmentRolePreference investmentRolePreference;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOURCE_INFO_ID")
    private SourceInfo sourceInfo;

    public SourceInfo getSourceInfo() {
        return sourceInfo;
    }

    public void setSourceInfo(SourceInfo sourceInfo) {
        this.sourceInfo = sourceInfo;
    }

    public InvestmentRolePreference getInvestmentRolePreference() {
        return investmentRolePreference;
    }

    public void setInvestmentRolePreference(InvestmentRolePreference investmentRolePreference) {
        this.investmentRolePreference = investmentRolePreference;
    }

    public InvestmentRiskPreference getInvestmentRiskPreference() {
        return investmentRiskPreference;
    }

    public void setInvestmentRiskPreference(InvestmentRiskPreference investmentRiskPreference) {
        this.investmentRiskPreference = investmentRiskPreference;
    }

    public List<UserInvestmentPreference> getInvestmentPreferenceList() {
        return investmentPreferenceList;
    }

    public void setInvestmentPreferenceList(List<UserInvestmentPreference> investmentPreferenceList) {
        this.investmentPreferenceList = investmentPreferenceList;
    }

    public List<UserInvestmentObjective> getInvestmentObjectiveList() {
        return investmentObjectiveList;
    }

    public void setInvestmentObjectiveList(List<UserInvestmentObjective> investmentObjectiveList) {
        this.investmentObjectiveList = investmentObjectiveList;
    }

    public InvestmentBudget getInvestmentBudget() {
        return investmentBudget;
    }

    public void setInvestmentBudget(InvestmentBudget investmentBudget) {
        this.investmentBudget = investmentBudget;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}