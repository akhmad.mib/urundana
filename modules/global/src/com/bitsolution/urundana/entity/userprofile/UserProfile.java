package com.bitsolution.urundana.entity.userprofile;

import com.bitsolution.urundana.entity.base.SnowflakeStandardEntity;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;
import com.haulmont.cuba.security.entity.User;

import javax.persistence.*;

@Table(name = "URUNDANA_USER_PROFILE")
@Entity(name = "urundana_UserProfile")
public class UserProfile extends SnowflakeStandardEntity {
    private static final long serialVersionUID = -278365254848408360L;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "USER_ID")
    @OnDeleteInverse(DeletePolicy.CASCADE)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "PERSONAL_DATA_ID")
    @OnDelete(DeletePolicy.CASCADE)
    private PersonalDataProfile personalData;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "JOB_ID")
    @OnDelete(DeletePolicy.CASCADE)
    private JobProfile job;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "DOCUMENT_ID")
    @OnDelete(DeletePolicy.CASCADE)
    private DocumentProfile document;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "BANK_ID")
    @OnDelete(DeletePolicy.CASCADE)
    private BankProfile bank;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "PREFERENCE_ID")
    @OnDeleteInverse(DeletePolicy.CASCADE)
    private PreferenceProfile preference;

    @Column(name = "STEP")
    private Integer step;

    @Column(name = "HAS_FINISH")
    private Boolean hasFinish;

    public PreferenceProfile getPreference() {
        return preference;
    }

    public void setPreference(PreferenceProfile preference) {
        this.preference = preference;
    }

    public Boolean getHasFinish() {
        return hasFinish;
    }

    public void setHasFinish(Boolean hasFinish) {
        this.hasFinish = hasFinish;
    }

    public Integer getStep() {
        return step;
    }

    public void setStep(Integer step) {
        this.step = step;
    }

    public BankProfile getBank() {
        return bank;
    }

    public void setBank(BankProfile bank) {
        this.bank = bank;
    }

    public DocumentProfile getDocument() {
        return document;
    }

    public void setDocument(DocumentProfile document) {
        this.document = document;
    }

    public JobProfile getJob() {
        return job;
    }

    public void setJob(JobProfile job) {
        this.job = job;
    }

    public PersonalDataProfile getPersonalData() {
        return personalData;
    }

    public void setPersonalData(PersonalDataProfile personalData) {
        this.personalData = personalData;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}