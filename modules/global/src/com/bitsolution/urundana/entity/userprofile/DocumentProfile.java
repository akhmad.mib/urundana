package com.bitsolution.urundana.entity.userprofile;

import com.bitsolution.urundana.entity.base.SnowflakeStandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.FileDescriptor;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;
import com.haulmont.cuba.security.entity.User;

import javax.persistence.*;
import java.util.Date;

@Table(name = "URUNDANA_DOCUMENT_PROFILE")
@Entity(name = "urundana_DocumentProfile")
@NamePattern("%s|idCardNumber")
public class DocumentProfile extends SnowflakeStandardEntity {
    private static final long serialVersionUID = 5945435338175851761L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    @OnDeleteInverse(DeletePolicy.CASCADE)
    private User user;

    @Column(name = "SID_NUMBER", length = 100)
    private String sidNumber;

    @Column(name = "ID_CARD_NUMBER", length = 100)
    private String idCardNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_CARD_PHOTO_ID")
    private FileDescriptor idCardPhoto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SELFIE_PHOTO_ID")
    private FileDescriptor selfiePhoto;

    @Column(name = "NPWP_NUMBER", length = 100)
    private String npwpNumber;

    @Temporal(TemporalType.DATE)
    @Column(name = "NPWP_DATE")
    private Date npwpDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NPWP_PHOTO_ID")
    private FileDescriptor npwpPhoto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "KK_PHOTO_ID")
    private FileDescriptor kkPhoto;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public FileDescriptor getKkPhoto() {
        return kkPhoto;
    }

    public void setKkPhoto(FileDescriptor kkPhoto) {
        this.kkPhoto = kkPhoto;
    }

    public FileDescriptor getNpwpPhoto() {
        return npwpPhoto;
    }

    public void setNpwpPhoto(FileDescriptor npwpPhoto) {
        this.npwpPhoto = npwpPhoto;
    }

    public Date getNpwpDate() {
        return npwpDate;
    }

    public void setNpwpDate(Date npwpDate) {
        this.npwpDate = npwpDate;
    }

    public String getNpwpNumber() {
        return npwpNumber;
    }

    public void setNpwpNumber(String npwpNumber) {
        this.npwpNumber = npwpNumber;
    }

    public FileDescriptor getSelfiePhoto() {
        return selfiePhoto;
    }

    public void setSelfiePhoto(FileDescriptor selfiePhoto) {
        this.selfiePhoto = selfiePhoto;
    }

    public FileDescriptor getIdCardPhoto() {
        return idCardPhoto;
    }

    public void setIdCardPhoto(FileDescriptor idCardPhoto) {
        this.idCardPhoto = idCardPhoto;
    }

    public String getIdCardNumber() {
        return idCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        this.idCardNumber = idCardNumber;
    }

    public String getSidNumber() {
        return sidNumber;
    }

    public void setSidNumber(String sidNumber) {
        this.sidNumber = sidNumber;
    }
}