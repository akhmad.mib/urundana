package com.bitsolution.urundana.entity.userprofile;

import com.bitsolution.urundana.entity.base.BaseSnowflakeIdEntity;
import com.bitsolution.urundana.entity.master.InvestmentObjective;
import com.haulmont.cuba.security.entity.User;

import javax.persistence.*;

@Table(name = "URUNDANA_USER_INVESTMENT_OBJECTIVE")
@Entity(name = "urundana_UserInvestmentObjective")
public class UserInvestmentObjective extends BaseSnowflakeIdEntity {
    private static final long serialVersionUID = 9040974034403648386L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INVESTMENT_OBJECTIVE_ID")
    private InvestmentObjective investmentObjective;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREFERENCE_PROFILE_ID")
    private PreferenceProfile preferenceProfile;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROFILE_ID")
    private Profile profile;

    public PreferenceProfile getPreferenceProfile() {
        return preferenceProfile;
    }

    public void setPreferenceProfile(PreferenceProfile preferenceProfile) {
        this.preferenceProfile = preferenceProfile;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public InvestmentObjective getInvestmentObjective() {
        return investmentObjective;
    }

    public void setInvestmentObjective(InvestmentObjective investmentObjective) {
        this.investmentObjective = investmentObjective;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}