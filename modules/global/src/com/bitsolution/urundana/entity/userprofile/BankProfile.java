package com.bitsolution.urundana.entity.userprofile;

import com.bitsolution.urundana.entity.base.SnowflakeStandardEntity;
import com.bitsolution.urundana.entity.master.Bank;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;
import com.haulmont.cuba.security.entity.User;

import javax.persistence.*;

@Table(name = "URUNDANA_BANK_PROFILE")
@Entity(name = "urundana_BankProfile")
@NamePattern("%s|bank")
public class BankProfile extends SnowflakeStandardEntity {
    private static final long serialVersionUID = -93587630213967406L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    @OnDeleteInverse(DeletePolicy.CASCADE)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BANK_ID")
    private Bank bank;

    @Column(name = "BRANCH_NAME", length = 100)
    private String branchName;

    @Column(name = "OWNER_NAME", length = 100)
    private String ownerName;

    @Column(name = "ACCOUNT_NUMBER")
    private Long accountNumber;

    @Column(name = "MOTHER_NAME", length = 100)
    private String motherName;

    @Column(name = "HEIRS_NAME", length = 100)
    private String heirsName;

    @Column(name = "HEIR_RELATIONSHIP")
    private String heirRelationship;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getHeirRelationship() {
        return heirRelationship;
    }

    public void setHeirRelationship(String heirRelationship) {
        this.heirRelationship = heirRelationship;
    }

    public String getHeirsName() {
        return heirsName;
    }

    public void setHeirsName(String heirsName) {
        this.heirsName = heirsName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }
}