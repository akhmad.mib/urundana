package com.bitsolution.urundana.entity.userprofile;

import com.bitsolution.urundana.entity.base.SnowflakeStandardEntity;
import com.bitsolution.urundana.entity.master.*;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;
import com.haulmont.cuba.security.entity.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Table(name = "URUNDANA_PERSONAL_DATA_PROFILE")
@Entity(name = "urundana_PersonalDataProfile")
@NamePattern("%s|firstName")
public class PersonalDataProfile extends SnowflakeStandardEntity {
    private static final long serialVersionUID = 5175086252943025186L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    @OnDeleteInverse(DeletePolicy.CASCADE)
    private User user;

    @NotNull
    @Column(name = "FIRST_NAME", nullable = false, length = 50)
    private String firstName;

    @Column(name = "LAST_NAME", length = 50)
    private String lastName;

    @JoinColumn(name = "GENDER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Gender gender;

    @Column(name = "BIRTH_OF_PLACE", length = 100)
    private String birthOfPlace;

    @Temporal(TemporalType.DATE)
    @Column(name = "BIRTH_OF_DATE")
    private Date birthOfDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NATIONALITY_ID")
    private Nationality nationality;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MARITAL_STATUS_ID")
    private MaritalStatus maritalStatus;

    @Column(name = "MOTHER_NAME", length = 100)
    private String motherName;

    @Lob
    @Column(name = "PROFIL_BACKGROUND")
    private String profilBackground;

    @Column(name = "PHONE_NUMBER", length = 15)
    private String phoneNumber;

    @Column(name = "LANDLINE_NUMBER", length = 15)
    private String landlineNumber;

    @Lob
    @Column(name = "ID_CARD_ADDRESS")
    private String idCardAddress;

    @JoinColumn(name = "ID_CARD_PROVINCE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Province idCardProvince;

    @JoinColumn(name = "ID_CARD_CITY_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private City idCardCity;

    @JoinColumn(name = "ID_CARD_SUB_DISTRICT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private SubDistrict idCardSubDistrict;

    @Column(name = "ID_CARD_URBAN_VILLAGE", length = 100)
    private String idCardUrbanVillage;

    @Column(name = "ID_CARD_RT")
    private Integer idCardRT;

    @Column(name = "ID_CARD_RW")
    private Integer idCardRW;

    @Lob
    @Column(name = "HOME_ADDRESS")
    private String homeAddress;

    @JoinColumn(name = "HOME_PROVINCE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Province homeProvince;

    @JoinColumn(name = "HOME_CITY_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private City homeCity;

    @JoinColumn(name = "HOME_SUB_DISTRICT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private SubDistrict homeSubDistrict;

    @Column(name = "HOME_URBAN_VILLAGE", length = 100)
    private String homeUrbanVillage;

    @Column(name = "HOME_RT")
    private Integer homeRT;

    @Column(name = "HOME_RW")
    private Integer homeRW;

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setLandlineNumber(String landlineNumber) {
        this.landlineNumber = landlineNumber;
    }

    public String getLandlineNumber() {
        return landlineNumber;
    }

    public void setHomeSubDistrict(SubDistrict homeSubDistrict) {
        this.homeSubDistrict = homeSubDistrict;
    }

    public SubDistrict getHomeSubDistrict() {
        return homeSubDistrict;
    }

    public void setHomeCity(City homeCity) {
        this.homeCity = homeCity;
    }

    public City getHomeCity() {
        return homeCity;
    }

    public void setHomeProvince(Province homeProvince) {
        this.homeProvince = homeProvince;
    }

    public Province getHomeProvince() {
        return homeProvince;
    }

    public void setIdCardSubDistrict(SubDistrict idCardSubDistrict) {
        this.idCardSubDistrict = idCardSubDistrict;
    }

    public SubDistrict getIdCardSubDistrict() {
        return idCardSubDistrict;
    }

    public void setIdCardCity(City idCardCity) {
        this.idCardCity = idCardCity;
    }

    public City getIdCardCity() {
        return idCardCity;
    }

    public void setIdCardProvince(Province idCardProvince) {
        this.idCardProvince = idCardProvince;
    }

    public Province getIdCardProvince() {
        return idCardProvince;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getHomeRW() {
        return homeRW;
    }

    public void setHomeRW(Integer homeRW) {
        this.homeRW = homeRW;
    }

    public Integer getHomeRT() {
        return homeRT;
    }

    public void setHomeRT(Integer homeRT) {
        this.homeRT = homeRT;
    }

    public String getHomeUrbanVillage() {
        return homeUrbanVillage;
    }

    public void setHomeUrbanVillage(String homeUrbanVillage) {
        this.homeUrbanVillage = homeUrbanVillage;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public Integer getIdCardRW() {
        return idCardRW;
    }

    public void setIdCardRW(Integer idCardRW) {
        this.idCardRW = idCardRW;
    }

    public Integer getIdCardRT() {
        return idCardRT;
    }

    public void setIdCardRT(Integer idCardRT) {
        this.idCardRT = idCardRT;
    }

    public String getIdCardUrbanVillage() {
        return idCardUrbanVillage;
    }

    public void setIdCardUrbanVillage(String idCardUrbanVillage) {
        this.idCardUrbanVillage = idCardUrbanVillage;
    }

    public String getIdCardAddress() {
        return idCardAddress;
    }

    public void setIdCardAddress(String idCardAddress) {
        this.idCardAddress = idCardAddress;
    }

    public String getProfilBackground() {
        return profilBackground;
    }

    public void setProfilBackground(String profilBackground) {
        this.profilBackground = profilBackground;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Gender getGender() {
        return gender;
    }

    public Nationality getNationality() {
        return nationality;
    }

    public void setNationality(Nationality nationality) {
        this.nationality = nationality;
    }

    public Date getBirthOfDate() {
        return birthOfDate;
    }

    public void setBirthOfDate(Date birthOfDate) {
        this.birthOfDate = birthOfDate;
    }

    public String getBirthOfPlace() {
        return birthOfPlace;
    }

    public void setBirthOfPlace(String birthOfPlace) {
        this.birthOfPlace = birthOfPlace;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}