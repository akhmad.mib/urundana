package com.bitsolution.urundana.entity.userprofile;

import com.bitsolution.urundana.entity.base.SnowflakeStandardEntity;
import com.bitsolution.urundana.entity.master.*;
import com.haulmont.cuba.core.entity.FileDescriptor;
import com.haulmont.cuba.security.entity.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Table(name = "URUNDANA_PROFILE")
@Entity(name = "urundana_Profile")
public class Profile extends SnowflakeStandardEntity {
    private static final long serialVersionUID = -8411732270165837660L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    private User user;

    @NotNull
    @Column(name = "FIRST_NAME", nullable = false, length = 50)
    private String firstName;

    @Column(name = "LAST_NAME", length = 50)
    private String lastName;

    @JoinColumn(name = "GENDER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Gender gender;

    @Column(name = "BIRTH_OF_PLACE", length = 100)
    private String birthOfPlace;

    @Temporal(TemporalType.DATE)
    @Column(name = "BIRTH_OF_DATE")
    private Date birthOfDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NATIONALITY_ID")
    private Nationality nationality;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MARITAL_STATUS_ID")
    private MaritalStatus maritalStatus;

    @Lob
    @Column(name = "PROFIL_BACKGROUND")
    private String profilBackground;

    @Column(name = "PHONE_NUMBER", length = 15)
    private String phoneNumber;

    @Column(name = "LANDLINE_NUMBER", length = 15)
    private String landlineNumber;

    @Lob
    @Column(name = "ID_CARD_ADDRESS")
    private String idCardAddress;

    @JoinColumn(name = "ID_CARD_PROVINCE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Province idCardProvince;

    @JoinColumn(name = "ID_CARD_CITY_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private City idCardCity;

    @JoinColumn(name = "ID_CARD_SUB_DISTRICT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private SubDistrict idCardSubDistrict;

    @Column(name = "ID_CARD_URBAN_VILLAGE", length = 100)
    private String idCardUrbanVillage;

    @Column(name = "ID_CARD_RT")
    private Integer idCardRT;

    @Column(name = "ID_CARD_RW")
    private Integer idCardRW;

    @Lob
    @Column(name = "HOME_ADDRESS")
    private String homeAddress;

    @JoinColumn(name = "HOME_PROVINCE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Province homeProvince;

    @JoinColumn(name = "HOME_CITY_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private City homeCity;

    @JoinColumn(name = "HOME_SUB_DISTRICT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private SubDistrict homeSubDistrict;

    @Column(name = "HOME_URBAN_VILLAGE", length = 100)
    private String homeUrbanVillage;

    @Column(name = "HOME_RT")
    private Integer homeRT;

    @Column(name = "HOME_RW")
    private Integer homeRW;

    //job
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LAST_EDUCATION_ID")
    private Education lastEducation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "JOB_ID")
    private Job job;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INDUSTRY_CATEGORY_ID")
    private IndustryCategory industryCategory;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INCOME_ID")
    private Income income;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INCOME_SOURCE_ID")
    private IncomeSource incomeSource;

    @Lob
    @Column(name = "BUSINESS_DESCRIPTION")
    private String businessDescription;

    @Lob
    @Column(name = "INCOME_SOURCE_DESCRIPTION")
    private String incomeSourceDescription;


    //dokumen
    @Column(name = "SID_NUMBER", length = 100)
    private String sidNumber;

    @Column(name = "ID_CARD_NUMBER", length = 100)
    private String idCardNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_CARD_PHOTO_ID")
    private FileDescriptor idCardPhoto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SELFIE_PHOTO_ID")
    private FileDescriptor selfiePhoto;

    @Column(name = "NPWP_NUMBER", length = 100)
    private String npwpNumber;

    @Temporal(TemporalType.DATE)
    @Column(name = "NPWP_DATE")
    private Date npwpDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NPWP_PHOTO_ID")
    private FileDescriptor npwpPhoto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "KK_PHOTO_ID")
    private FileDescriptor kkPhoto;

    //bank
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BANK_ID")
    private Bank bank;

    @Column(name = "BRANCH_NAME", length = 100)
    private String branchName;

    @Column(name = "OWNER_NAME", length = 100)
    private String ownerName;

    @Column(name = "ACCOUNT_NUMBER")
    private Long accountNumber;

    @Column(name = "MOTHER_NAME", length = 100)
    private String motherName;

    @Column(name = "HEIRS_NAME", length = 100)
    private String heirsName;

    @Column(name = "HEIR_RELATIONSHIP")
    private String heirRelationship;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INVESTMENT_BUDGET_ID")
    private InvestmentBudget investmentBudget;

    @OneToMany(mappedBy = "profile")
    private List<UserInvestmentObjective> investmentObjectiveList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INVESTMENT_RISK_PREFERENCE_ID")
    private InvestmentRiskPreference investmentRiskPreference;

    @OneToMany(mappedBy = "profile")
    private List<UserInvestmentPreference> investmentPreferenceList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INVESTMENT_ROLE_PREFERENCE_ID")
    private InvestmentRolePreference investmentRolePreference;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOURCE_INFO_ID")
    private SourceInfo sourceInfo;

    public void setLandlineNumber(String landlineNumber) {
        this.landlineNumber = landlineNumber;
    }

    public String getLandlineNumber() {
        return landlineNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setHomeSubDistrict(SubDistrict homeSubDistrict) {
        this.homeSubDistrict = homeSubDistrict;
    }

    public SubDistrict getHomeSubDistrict() {
        return homeSubDistrict;
    }

    public void setHomeCity(City homeCity) {
        this.homeCity = homeCity;
    }

    public City getHomeCity() {
        return homeCity;
    }

    public void setHomeProvince(Province homeProvince) {
        this.homeProvince = homeProvince;
    }

    public Province getHomeProvince() {
        return homeProvince;
    }

    public void setIdCardSubDistrict(SubDistrict idCardSubDistrict) {
        this.idCardSubDistrict = idCardSubDistrict;
    }

    public SubDistrict getIdCardSubDistrict() {
        return idCardSubDistrict;
    }

    public void setIdCardCity(City idCardCity) {
        this.idCardCity = idCardCity;
    }

    public City getIdCardCity() {
        return idCardCity;
    }

    public void setIdCardProvince(Province idCardProvince) {
        this.idCardProvince = idCardProvince;
    }

    public Province getIdCardProvince() {
        return idCardProvince;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getHomeRW() {
        return homeRW;
    }

    public void setHomeRW(Integer homeRW) {
        this.homeRW = homeRW;
    }

    public Integer getHomeRT() {
        return homeRT;
    }

    public void setHomeRT(Integer homeRT) {
        this.homeRT = homeRT;
    }

    public String getHomeUrbanVillage() {
        return homeUrbanVillage;
    }

    public void setHomeUrbanVillage(String homeUrbanVillage) {
        this.homeUrbanVillage = homeUrbanVillage;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public Integer getIdCardRW() {
        return idCardRW;
    }

    public void setIdCardRW(Integer idCardRW) {
        this.idCardRW = idCardRW;
    }

    public Integer getIdCardRT() {
        return idCardRT;
    }

    public void setIdCardRT(Integer idCardRT) {
        this.idCardRT = idCardRT;
    }

    public String getIdCardUrbanVillage() {
        return idCardUrbanVillage;
    }

    public void setIdCardUrbanVillage(String idCardUrbanVillage) {
        this.idCardUrbanVillage = idCardUrbanVillage;
    }

    public String getIdCardAddress() {
        return idCardAddress;
    }

    public void setIdCardAddress(String idCardAddress) {
        this.idCardAddress = idCardAddress;
    }

    public String getProfilBackground() {
        return profilBackground;
    }

    public void setProfilBackground(String profilBackground) {
        this.profilBackground = profilBackground;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Gender getGender() {
        return gender;
    }

    public Nationality getNationality() {
        return nationality;
    }

    public void setNationality(Nationality nationality) {
        this.nationality = nationality;
    }

    public Date getBirthOfDate() {
        return birthOfDate;
    }

    public void setBirthOfDate(Date birthOfDate) {
        this.birthOfDate = birthOfDate;
    }

    public String getBirthOfPlace() {
        return birthOfPlace;
    }

    public void setBirthOfPlace(String birthOfPlace) {
        this.birthOfPlace = birthOfPlace;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    //job
    public String getIncomeSourceDescription() {
        return incomeSourceDescription;
    }

    public void setIncomeSourceDescription(String incomeSourceDescription) {
        this.incomeSourceDescription = incomeSourceDescription;
    }

    public String getBusinessDescription() {
        return businessDescription;
    }

    public void setBusinessDescription(String businessDescription) {
        this.businessDescription = businessDescription;
    }

    public IncomeSource getIncomeSource() {
        return incomeSource;
    }

    public void setIncomeSource(IncomeSource incomeSource) {
        this.incomeSource = incomeSource;
    }

    public Income getIncome() {
        return income;
    }

    public void setIncome(Income income) {
        this.income = income;
    }

    public IndustryCategory getIndustryCategory() {
        return industryCategory;
    }

    public void setIndustryCategory(IndustryCategory industryCategory) {
        this.industryCategory = industryCategory;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Education getLastEducation() {
        return lastEducation;
    }

    public void setLastEducation(Education lastEducation) {
        this.lastEducation = lastEducation;
    }

    //dokumen
    public FileDescriptor getKkPhoto() {
        return kkPhoto;
    }

    public void setKkPhoto(FileDescriptor kkPhoto) {
        this.kkPhoto = kkPhoto;
    }

    public FileDescriptor getNpwpPhoto() {
        return npwpPhoto;
    }

    public void setNpwpPhoto(FileDescriptor npwpPhoto) {
        this.npwpPhoto = npwpPhoto;
    }

    public Date getNpwpDate() {
        return npwpDate;
    }

    public void setNpwpDate(Date npwpDate) {
        this.npwpDate = npwpDate;
    }

    public String getNpwpNumber() {
        return npwpNumber;
    }

    public void setNpwpNumber(String npwpNumber) {
        this.npwpNumber = npwpNumber;
    }

    public FileDescriptor getSelfiePhoto() {
        return selfiePhoto;
    }

    public void setSelfiePhoto(FileDescriptor selfiePhoto) {
        this.selfiePhoto = selfiePhoto;
    }

    public FileDescriptor getIdCardPhoto() {
        return idCardPhoto;
    }

    public void setIdCardPhoto(FileDescriptor idCardPhoto) {
        this.idCardPhoto = idCardPhoto;
    }

    public String getIdCardNumber() {
        return idCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        this.idCardNumber = idCardNumber;
    }

    public String getSidNumber() {
        return sidNumber;
    }

    public void setSidNumber(String sidNumber) {
        this.sidNumber = sidNumber;
    }

    //bank
    public String getHeirRelationship() {
        return heirRelationship;
    }

    public void setHeirRelationship(String heirRelationship) {
        this.heirRelationship = heirRelationship;
    }

    public String getHeirsName() {
        return heirsName;
    }

    public void setHeirsName(String heirsName) {
        this.heirsName = heirsName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    //preference
    public SourceInfo getSourceInfo() {
        return sourceInfo;
    }

    public void setSourceInfo(SourceInfo sourceInfo) {
        this.sourceInfo = sourceInfo;
    }

    public InvestmentRolePreference getInvestmentRolePreference() {
        return investmentRolePreference;
    }

    public void setInvestmentRolePreference(InvestmentRolePreference investmentRolePreference) {
        this.investmentRolePreference = investmentRolePreference;
    }

    public InvestmentRiskPreference getInvestmentRiskPreference() {
        return investmentRiskPreference;
    }

    public void setInvestmentRiskPreference(InvestmentRiskPreference investmentRiskPreference) {
        this.investmentRiskPreference = investmentRiskPreference;
    }

    public List<UserInvestmentPreference> getInvestmentPreferenceList() {
        return investmentPreferenceList;
    }

    public void setInvestmentPreferenceList(List<UserInvestmentPreference> investmentPreferenceList) {
        this.investmentPreferenceList = investmentPreferenceList;
    }

    public List<UserInvestmentObjective> getInvestmentObjectiveList() {
        return investmentObjectiveList;
    }

    public void setInvestmentObjectiveList(List<UserInvestmentObjective> investmentObjectiveList) {
        this.investmentObjectiveList = investmentObjectiveList;
    }

    public InvestmentBudget getInvestmentBudget() {
        return investmentBudget;
    }

    public void setInvestmentBudget(InvestmentBudget investmentBudget) {
        this.investmentBudget = investmentBudget;
    }
}