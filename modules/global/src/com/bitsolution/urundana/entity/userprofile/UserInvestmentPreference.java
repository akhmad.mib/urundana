package com.bitsolution.urundana.entity.userprofile;

import com.bitsolution.urundana.entity.base.BaseSnowflakeIdEntity;
import com.bitsolution.urundana.entity.master.IndustryCategory;
import com.haulmont.cuba.security.entity.User;

import javax.persistence.*;

@Table(name = "URUNDANA_USER_INVESTMENT_PREFERENCE")
@Entity(name = "urundana_UserInvestmentPreference")
public class UserInvestmentPreference extends BaseSnowflakeIdEntity {
    private static final long serialVersionUID = -7819539638693966262L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INVESTMENT_PREFERENCE_ID")
    private IndustryCategory investmentPreference;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREFERENCE_PROFILE_ID")
    private PreferenceProfile preferenceProfile;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROFILE_ID")
    private Profile profile;

    public PreferenceProfile getPreferenceProfile() {
        return preferenceProfile;
    }

    public void setPreferenceProfile(PreferenceProfile preferenceProfile) {
        this.preferenceProfile = preferenceProfile;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public IndustryCategory getInvestmentPreference() {
        return investmentPreference;
    }

    public void setInvestmentPreference(IndustryCategory investmentPreference) {
        this.investmentPreference = investmentPreference;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}