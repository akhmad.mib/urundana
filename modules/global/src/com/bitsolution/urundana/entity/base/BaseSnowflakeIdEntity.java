package com.bitsolution.urundana.entity.base;

import com.haulmont.chile.core.annotations.MetaClass;
import com.haulmont.cuba.core.entity.BaseGenericIdEntity;
import com.haulmont.cuba.core.entity.HasUuid;
import com.haulmont.cuba.core.entity.annotation.UnavailableInSecurityConstraints;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@MappedSuperclass
@MetaClass(name = "sys$BaseSnowflakeIdEntity")
//@MetaClass(name = "sys$BaseLongIdEntity")
@UnavailableInSecurityConstraints
public abstract class BaseSnowflakeIdEntity extends BaseGenericIdEntity<String> implements HasUuid {

    private static final long serialVersionUID = 1748237513475338490L;

    @Id
    @Column(name = "ID", nullable = false)
    protected String id;

    @Column(name = "UUID")
    private UUID uuid;

    public BaseSnowflakeIdEntity() {
        id = Long.toHexString(new Snowflake(1).nextId()) + "-" + UUID.randomUUID().toString().substring(15);
//        id = UUID.fromString(Long.toHexString(new Snowflake(1).nextId()) + "-" + UUID.randomUUID().toString().substring(15));
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }
}