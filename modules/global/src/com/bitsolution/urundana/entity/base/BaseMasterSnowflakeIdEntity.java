package com.bitsolution.urundana.entity.base;

import com.haulmont.cuba.core.entity.HasUuid;
import com.haulmont.cuba.core.entity.annotation.UnavailableInSecurityConstraints;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@MappedSuperclass
@UnavailableInSecurityConstraints
public class BaseMasterSnowflakeIdEntity extends BaseSnowflakeIdEntity implements HasUuid {

    @Column(name = "CODE", length = 100)
    private String code;

    @NotNull
    @Column(name = "LABEL", nullable = false, length = 100)
    private String label;

    @Column(name = "LABEL2ND", length = 100)
    private String label2nd;

    @Lob
    @Column(name = "DESCRIPTION")
    private String description;

    @Lob
    @Column(name = "DESCRIPTION2ND")
    private String description2nd;

    public String getDescription2nd() {
        return description2nd;
    }

    public void setDescription2nd(String description2nd) {
        this.description2nd = description2nd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLabel2nd() {
        return label2nd;
    }

    public void setLabel2nd(String label2nd) {
        this.label2nd = label2nd;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
