package com.bitsolution.urundana.entity.businessprofile;

import com.bitsolution.urundana.entity.base.SnowflakeStandardEntity;
import com.bitsolution.urundana.entity.enums.ApprovalStatus;
import com.bitsolution.urundana.entity.enums.CapitalFundingProgressStatus;
import com.bitsolution.urundana.entity.master.*;
import com.haulmont.cuba.security.entity.User;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Table(name = "URUNDANA_CAPITAL_FUNDING")
@Entity(name = "urundana_CapitalFunding")
public class CapitalFunding extends SnowflakeStandardEntity {
    private static final long serialVersionUID = 7443138556416014994L;

    @Column(name = "FULL_NAME", length = 100)
    private String fullName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "JOB_POSITION_ID")
    private JobPosition jobPosition;

    @Column(name = "JOB_POSITION_OTHERS")
    private String jobPositionOthers;

    @Column(name = "EMAIL", length = 150)
    private String email;

    @Column(name = "WHATSAPP_NUMBER", length = 15)
    private String whatsappNumber;

    @Column(name = "BUSINESS_BRAND", length = 150)
    private String businessBrand;

    @OneToMany(mappedBy = "capitalFunding")
    private List<IndustryCategory> industryCategory;

    @Column(name = "INDUSTRY_CATEGORY_OTHERS", length = 150)
    private String industryCategoryOthers;

    @Column(name = "PRODUCT_OFFERED")
    private String productOffered;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FORM_OF_BUSINESS_ID")
    private FormOfBusiness formOfBusiness;

    @Column(name = "FORM_OF_BUSINESS_OTHERS")
    private String formOfBusinessOthers;

    @Column(name = "YEAR_OF_BUSINESS")
    private Integer yearOfBusiness;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MANY_BRANCH_OF_BUSINESS_ID")
    private ManyBranchOfBusiness manyBranchOfBusiness;

    @Column(name = "MANY_BRANCH_OF_BUSINESS_OTHERS", length = 150)
    private String manyBranchOfBusinessOthers;

    @Lob
    @Column(name = "BUSINESS_DESCRIPTION")
    private String businessDescription;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MARKETING_FUNNEL_ID")
    private MarketingFunnel marketingFunnel;

    @Column(name = "OMZET_PER_MONTH")
    private Long omzetPerMonth;

    @Column(name = "PROFIT_PER_MONTH")
    private Long profitPerMonth;

    @Lob
    @Column(name = "BUSINESS_PLAN")
    private String businessPlan;

    @Column(name = "AMOUNT_OF_FUNDING")
    private Long amountOfFunding;

    @Lob
    @Column(name = "OPERATIONAL_PLAN")
    private String operationalPlan;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOURCE_INFO_ID")
    private SourceInfo sourceInfo;

    @Column(name = "APPROVAL_STATUS")
    private Integer approvalStatus;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "APPROVAL_ACTION_TIME")
    private Date approvalActionTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "APPROVAL_ACTION_BY_ID")
    private User approvalActionBy;

    @Column(name = "CAPITAL_FUNDING_PROGRESS_STATUS")
    private Integer capitalFundingProgressStatus;

    @Temporal(TemporalType.DATE)
    @Column(name = "PRE_ORDER_DATE")
    private Date preOrderDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "STOCK_OFFERING_DATE")
    private Date stockOfferingDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "FUNDING_FULFILLED")
    private Date fundingFulfilled;

    @Temporal(TemporalType.DATE)
    @Column(name = "SUBMISSION_OF_FUNDS_DATE")
    private Date submissionOfFundsDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "DIVIDEND_DISTRIBUTION_DATE")
    private Date dividendDistributionDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "TARGET_FUNDING_FULFILLED_DATE")
    private Date targetFundingFulfilledDate;

    public void setIndustryCategory(List<IndustryCategory> industryCategory) {
        this.industryCategory = industryCategory;
    }

    public List<IndustryCategory> getIndustryCategory() {
        return industryCategory;
    }

    public Date getTargetFundingFulfilledDate() {
        return targetFundingFulfilledDate;
    }

    public void setTargetFundingFulfilledDate(Date targetFundingFulfilledDate) {
        this.targetFundingFulfilledDate = targetFundingFulfilledDate;
    }

    public Date getDividendDistributionDate() {
        return dividendDistributionDate;
    }

    public void setDividendDistributionDate(Date dividendDistributionDate) {
        this.dividendDistributionDate = dividendDistributionDate;
    }

    public Date getSubmissionOfFundsDate() {
        return submissionOfFundsDate;
    }

    public void setSubmissionOfFundsDate(Date submissionOfFundsDate) {
        this.submissionOfFundsDate = submissionOfFundsDate;
    }

    public Date getFundingFulfilled() {
        return fundingFulfilled;
    }

    public void setFundingFulfilled(Date fundingFulfilled) {
        this.fundingFulfilled = fundingFulfilled;
    }

    public Date getStockOfferingDate() {
        return stockOfferingDate;
    }

    public void setStockOfferingDate(Date stockOfferingDate) {
        this.stockOfferingDate = stockOfferingDate;
    }

    public Date getPreOrderDate() {
        return preOrderDate;
    }

    public void setPreOrderDate(Date preOrderDate) {
        this.preOrderDate = preOrderDate;
    }

    public CapitalFundingProgressStatus getCapitalFundingProgressStatus() {
        return capitalFundingProgressStatus == null ? null : CapitalFundingProgressStatus.fromId(capitalFundingProgressStatus);
    }

    public void setCapitalFundingProgressStatus(CapitalFundingProgressStatus capitalFundingProgressStatus) {
        this.capitalFundingProgressStatus = capitalFundingProgressStatus == null ? null : capitalFundingProgressStatus.getId();
    }

    public User getApprovalActionBy() {
        return approvalActionBy;
    }

    public void setApprovalActionBy(User approvalActionBy) {
        this.approvalActionBy = approvalActionBy;
    }

    public Date getApprovalActionTime() {
        return approvalActionTime;
    }

    public void setApprovalActionTime(Date approvalActionTime) {
        this.approvalActionTime = approvalActionTime;
    }

    public ApprovalStatus getApprovalStatus() {
        return approvalStatus == null ? null : ApprovalStatus.fromId(approvalStatus);
    }

    public void setApprovalStatus(ApprovalStatus approvalStatus) {
        this.approvalStatus = approvalStatus == null ? null : approvalStatus.getId();
    }

    public SourceInfo getSourceInfo() {
        return sourceInfo;
    }

    public void setSourceInfo(SourceInfo sourceInfo) {
        this.sourceInfo = sourceInfo;
    }

    public String getOperationalPlan() {
        return operationalPlan;
    }

    public void setOperationalPlan(String operationalPlan) {
        this.operationalPlan = operationalPlan;
    }

    public Long getAmountOfFunding() {
        return amountOfFunding;
    }

    public void setAmountOfFunding(Long amountOfFunding) {
        this.amountOfFunding = amountOfFunding;
    }

    public String getBusinessPlan() {
        return businessPlan;
    }

    public void setBusinessPlan(String businessPlan) {
        this.businessPlan = businessPlan;
    }

    public Long getProfitPerMonth() {
        return profitPerMonth;
    }

    public void setProfitPerMonth(Long profitPerMonth) {
        this.profitPerMonth = profitPerMonth;
    }

    public Long getOmzetPerMonth() {
        return omzetPerMonth;
    }

    public void setOmzetPerMonth(Long omzetPerMonth) {
        this.omzetPerMonth = omzetPerMonth;
    }

    public MarketingFunnel getMarketingFunnel() {
        return marketingFunnel;
    }

    public void setMarketingFunnel(MarketingFunnel marketingFunnel) {
        this.marketingFunnel = marketingFunnel;
    }

    public String getBusinessDescription() {
        return businessDescription;
    }

    public void setBusinessDescription(String businessDescription) {
        this.businessDescription = businessDescription;
    }

    public String getManyBranchOfBusinessOthers() {
        return manyBranchOfBusinessOthers;
    }

    public void setManyBranchOfBusinessOthers(String manyBranchOfBusinessOthers) {
        this.manyBranchOfBusinessOthers = manyBranchOfBusinessOthers;
    }

    public ManyBranchOfBusiness getManyBranchOfBusiness() {
        return manyBranchOfBusiness;
    }

    public void setManyBranchOfBusiness(ManyBranchOfBusiness manyBranchOfBusiness) {
        this.manyBranchOfBusiness = manyBranchOfBusiness;
    }

    public Integer getYearOfBusiness() {
        return yearOfBusiness;
    }

    public void setYearOfBusiness(Integer yearOfBusiness) {
        this.yearOfBusiness = yearOfBusiness;
    }

    public String getFormOfBusinessOthers() {
        return formOfBusinessOthers;
    }

    public void setFormOfBusinessOthers(String formOfBusinessOthers) {
        this.formOfBusinessOthers = formOfBusinessOthers;
    }

    public FormOfBusiness getFormOfBusiness() {
        return formOfBusiness;
    }

    public void setFormOfBusiness(FormOfBusiness formOfBusiness) {
        this.formOfBusiness = formOfBusiness;
    }

    public String getProductOffered() {
        return productOffered;
    }

    public void setProductOffered(String productOffered) {
        this.productOffered = productOffered;
    }

    public String getIndustryCategoryOthers() {
        return industryCategoryOthers;
    }

    public void setIndustryCategoryOthers(String industryCategoryOthers) {
        this.industryCategoryOthers = industryCategoryOthers;
    }

    public String getBusinessBrand() {
        return businessBrand;
    }

    public void setBusinessBrand(String businessBrand) {
        this.businessBrand = businessBrand;
    }

    public String getWhatsappNumber() {
        return whatsappNumber;
    }

    public void setWhatsappNumber(String whatsappNumber) {
        this.whatsappNumber = whatsappNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getJobPositionOthers() {
        return jobPositionOthers;
    }

    public void setJobPositionOthers(String jobPositionOthers) {
        this.jobPositionOthers = jobPositionOthers;
    }

    public JobPosition getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(JobPosition jobPosition) {
        this.jobPosition = jobPosition;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}