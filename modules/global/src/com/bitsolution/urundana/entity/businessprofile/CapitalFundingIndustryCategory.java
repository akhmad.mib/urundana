package com.bitsolution.urundana.entity.businessprofile;

import com.bitsolution.urundana.entity.master.IndustryCategory;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;

import javax.persistence.*;

@Table(name = "URUNDANA_CAPITAL_FUNDING_JOB_INDUSTRY")
@Entity(name = "urundana_CapitalFundingJobIndustry")
public class CapitalFundingIndustryCategory extends BaseIntegerIdEntity {
    private static final long serialVersionUID = 11568887080550619L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CAPITAL_FUNDING_ID")
    private CapitalFunding capitalFunding;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INDUSTRY_CATEGORY_ID")
    private IndustryCategory industryCategory;

    public IndustryCategory getIndustryCategory() {
        return industryCategory;
    }

    public void setIndustryCategory(IndustryCategory industryCategory) {
        this.industryCategory = industryCategory;
    }

    public CapitalFunding getCapitalFunding() {
        return capitalFunding;
    }

    public void setCapitalFunding(CapitalFunding capitalFunding) {
        this.capitalFunding = capitalFunding;
    }
}