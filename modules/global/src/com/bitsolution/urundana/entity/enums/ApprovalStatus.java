package com.bitsolution.urundana.entity.enums;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum ApprovalStatus implements EnumClass<Integer> {

    PENDING(0),
    APPROVED(1),
    REJECTED(2);

    private Integer id;

    ApprovalStatus(Integer value) {
        this.id = value;
    }

    public Integer getId() {
        return id;
    }

    @Nullable
    public static ApprovalStatus fromId(Integer id) {
        for (ApprovalStatus at : ApprovalStatus.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}