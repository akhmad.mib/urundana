package com.bitsolution.urundana.entity.enums;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum CapitalFundingProgressStatus implements EnumClass<Integer> {

    PRE_ORDER(1),
    STOCK_OFFERING(2),
    FUNDING_FULFILLED(3),
    SUBMISSION_OF_FUNDS(4),
    DIVIDEND_DISTRIBUTION(5);

    private Integer id;

    CapitalFundingProgressStatus(Integer value) {
        this.id = value;
    }

    public Integer getId() {
        return id;
    }

    @Nullable
    public static CapitalFundingProgressStatus fromId(Integer id) {
        for (CapitalFundingProgressStatus at : CapitalFundingProgressStatus.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}