package com.bitsolution.urundana.entity.enums;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum TypeOfArea implements EnumClass<String> {

    KABUPATEN("1"),
    KOTA("2"),
    DESA("3"),
    KELURAHAN("4");

    private String id;

    TypeOfArea(String value) {
        this.id = value;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static TypeOfArea fromId(String id) {
        for (TypeOfArea at : TypeOfArea.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}