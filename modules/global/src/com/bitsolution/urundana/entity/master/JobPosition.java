package com.bitsolution.urundana.entity.master;

import com.bitsolution.urundana.entity.base.BaseMasterSnowflakeIdEntity;
import com.bitsolution.urundana.entity.base.BaseSnowflakeIdEntity;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "URUNDANA_JOB_POSITION")
@Entity(name = "urundana_JobPosition")
@NamePattern("%s|label")
public class JobPosition extends BaseMasterSnowflakeIdEntity {
    private static final long serialVersionUID = -8815861975522949241L;

}