package com.bitsolution.urundana.entity.master;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Table(name = "URUNDANA_SUB_DISTRICT")
@Entity(name = "urundana_SubDistrict")
@NamePattern("%s|name")
public class SubDistrict extends BaseIntegerIdEntity {
    private static final long serialVersionUID = -5432746352333170363L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROVINCE_ID")
    private Province province;

    @Column(name = "PROVINCE_NAME", length = 100)
    private String provinceName;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "CITY_ID")
    private City city;

    @Column(name = "CITY_NAME", length = 100)
    private String cityName;

    @NotNull
    @Column(name = "NAME", nullable = false, length = 100)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }
}