package com.bitsolution.urundana.entity.master;

import com.bitsolution.urundana.entity.base.BaseMasterSnowflakeIdEntity;
import com.bitsolution.urundana.entity.base.BaseSnowflakeIdEntity;
import com.haulmont.chile.core.annotations.NamePattern;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "URUNDANA_MARITAL_STATUS")
@Entity(name = "urundana_MaritalStatus")
@NamePattern("%s|label")
public class MaritalStatus extends BaseMasterSnowflakeIdEntity {
    private static final long serialVersionUID = -3318763256510055835L;

}