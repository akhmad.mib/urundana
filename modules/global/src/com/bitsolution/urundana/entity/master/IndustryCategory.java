package com.bitsolution.urundana.entity.master;

import com.bitsolution.urundana.entity.base.BaseMasterSnowflakeIdEntity;
import com.bitsolution.urundana.entity.businessprofile.CapitalFunding;
import com.haulmont.chile.core.annotations.NamePattern;

import javax.persistence.*;

@Table(name = "URUNDANA_INDUSTRY_CATEGORY")
@Entity(name = "urundana_IndustryCategory")
@NamePattern("%s|label")
public class IndustryCategory extends BaseMasterSnowflakeIdEntity {
    private static final long serialVersionUID = 8376577717748212091L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CAPITAL_FUNDING_ID")
    private CapitalFunding capitalFunding;

    public CapitalFunding getCapitalFunding() {
        return capitalFunding;
    }

    public void setCapitalFunding(CapitalFunding capitalFunding) {
        this.capitalFunding = capitalFunding;
    }
}