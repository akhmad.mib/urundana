package com.bitsolution.urundana.entity.master;

import com.bitsolution.urundana.entity.base.BaseMasterSnowflakeIdEntity;
import com.bitsolution.urundana.entity.base.BaseSnowflakeIdEntity;
import com.haulmont.chile.core.annotations.NamePattern;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "URUNDANA_EDUCATION")
@Entity(name = "urundana_Education")
@NamePattern("%s|label")
public class Education extends BaseMasterSnowflakeIdEntity {
    private static final long serialVersionUID = -6119629569109805761L;

}