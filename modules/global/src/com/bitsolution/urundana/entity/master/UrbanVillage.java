package com.bitsolution.urundana.entity.master;

import com.bitsolution.urundana.entity.enums.TypeOfArea;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Table(name = "URUNDANA_URBAN_VILLAGE")
@Entity(name = "urundana_UrbanVillage")
@NamePattern("%s|name")
public class UrbanVillage extends BaseIntegerIdEntity {
    private static final long serialVersionUID = -5432746352333170363L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROVINCE_ID")
    private Province province;

    @Column(name = "PROVINCE_NAME", length = 100)
    private String provinceName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CITY_ID")
    private City city;

    @Column(name = "CITY_NAME", length = 100)
    private String cityName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SUB_DISTRICT_ID")
    private SubDistrict subDistrict;

    @Column(name = "SUB_DISTRICT_NAME", length = 100)
    private String subDistrictName;

    @NotNull
    @Column(name = "NAME", nullable = false, length = 100)
    private String name;

    @Column(name = "TYPE_OF_AREA")
    private String typeOfArea;

    public SubDistrict getSubDistrict() {
        return subDistrict;
    }

    public void setSubDistrict(SubDistrict subDistrict) {
        this.subDistrict = subDistrict;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public TypeOfArea getTypeOfArea() {
        return typeOfArea == null ? null : TypeOfArea.fromId(typeOfArea);
    }

    public void setTypeOfArea(TypeOfArea typeOfArea) {
        this.typeOfArea = typeOfArea == null ? null : typeOfArea.getId();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

}