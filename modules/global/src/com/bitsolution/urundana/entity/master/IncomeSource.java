package com.bitsolution.urundana.entity.master;

import com.bitsolution.urundana.entity.base.BaseMasterSnowflakeIdEntity;
import com.bitsolution.urundana.entity.base.BaseSnowflakeIdEntity;
import com.haulmont.chile.core.annotations.NamePattern;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "URUNDANA_INCOME_SOURCE")
@Entity(name = "urundana_IncomeSource")
@NamePattern("%s|label")
public class IncomeSource extends BaseMasterSnowflakeIdEntity {
    private static final long serialVersionUID = 4796997244873875541L;

}