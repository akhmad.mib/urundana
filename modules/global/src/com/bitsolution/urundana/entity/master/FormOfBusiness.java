package com.bitsolution.urundana.entity.master;

import com.bitsolution.urundana.entity.base.BaseMasterSnowflakeIdEntity;
import com.bitsolution.urundana.entity.base.BaseSnowflakeIdEntity;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Table(name = "URUNDANA_FORM_OF_BUSINESS")
@Entity(name = "urundana_FormOfBusiness")
@NamePattern("%s|label")
public class FormOfBusiness extends BaseMasterSnowflakeIdEntity {
    private static final long serialVersionUID = 6703750243283760108L;
}