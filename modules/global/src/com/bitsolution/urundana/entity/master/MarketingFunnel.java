package com.bitsolution.urundana.entity.master;

import com.bitsolution.urundana.entity.base.BaseMasterSnowflakeIdEntity;
import com.bitsolution.urundana.entity.base.BaseSnowflakeIdEntity;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "URUNDANA_MARKETING_FUNNEL")
@Entity(name = "urundana_MarketingFunnel")
@NamePattern("%s|label")
public class MarketingFunnel extends BaseMasterSnowflakeIdEntity {
    private static final long serialVersionUID = 7711627780256724138L;

}