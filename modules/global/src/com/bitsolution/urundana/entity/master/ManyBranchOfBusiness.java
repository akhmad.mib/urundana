package com.bitsolution.urundana.entity.master;

import com.bitsolution.urundana.entity.base.BaseMasterSnowflakeIdEntity;
import com.bitsolution.urundana.entity.base.BaseSnowflakeIdEntity;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "URUNDANA_MANY_BRANCH_OF_BUSINESS")
@Entity(name = "urundana_ManyBranchOfBusiness")
@NamePattern("%s|label")
public class ManyBranchOfBusiness extends BaseMasterSnowflakeIdEntity {
    private static final long serialVersionUID = 6206847437000373140L;

}