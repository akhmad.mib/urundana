package com.bitsolution.urundana.entity.master;

import com.bitsolution.urundana.entity.enums.TypeOfArea;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Table(name = "URUNDANA_CITY")
@Entity(name = "urundana_City")
@NamePattern("%s|name")
public class City extends BaseIntegerIdEntity {
    private static final long serialVersionUID = -3074692001222777158L;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "PROVINCE_ID")
    private Province province;

    @Column(name = "PROVINCE_NAME", length = 100)
    private String provinceName;

    @NotNull
    @Column(name = "NAME", nullable = false, length = 100)
    private String name;

    @Column(name = "TYPE_OF_AREA")
    private String typeOfArea;

    public TypeOfArea getTypeOfArea() {
        return typeOfArea == null ? null : TypeOfArea.fromId(typeOfArea);
    }

    public void setTypeOfArea(TypeOfArea typeOfArea) {
        this.typeOfArea = typeOfArea == null ? null : typeOfArea.getId();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }
}