package com.bitsolution.urundana.entity.master;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "URUNDANA_PROVINCE")
@Entity(name = "urundana_Province")
@NamePattern("%s|name")
public class Province extends BaseIntegerIdEntity {
    private static final long serialVersionUID = -1388543051391258656L;

    @NotNull
    @Column(name = "NAME", nullable = false, length = 100)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}